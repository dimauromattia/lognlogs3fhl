\begin{thebibliography}{34}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi

\bibitem[{{Abdo} {et~al.}(2010){Abdo}, {Ackermann}, {Ajello}, {Antolini},
  {et~al.}}]{2010ApJ...720..435A}
{Abdo}, A.~A., {Ackermann}, M., {Ajello}, M., {Antolini}, E., {et~al.} 2010,
  \apj, 720, 435

\bibitem[{{Acero} {et~al.}(2015){Acero}, {Ackermann}, {Ajello},
  {et~al.}}]{2015ApJS..218...23A}
{Acero}, F., {Ackermann}, M., {Ajello}, M., {et~al.} 2015, \apjs, 218, 23

\bibitem[{Acero {et~al.}(2016)}]{Acero:2016qlg}
Acero, F. {et~al.} 2016, Astrophys. J. Suppl., 223, 26

\bibitem[{{Ackermann} {et~al.}(2017){Ackermann}, {Ajello}, {Albert},
  {et~al.}}]{FermiPass8GC}
{Ackermann}, M., {Ajello}, M., {Albert}, A., {et~al.} 2017

\bibitem[{{Ackermann} {et~al.}(2012){Ackermann}, {Ajello}, {Allafort},
  {et~al.}}]{2012ApJ...755..164A}
{Ackermann}, M., {Ajello}, M., {Allafort}, A., {et~al.} 2012, \apj, 755, 164

\bibitem[{Ackermann {et~al.}(2013)}]{Ackermann:2013fwa}
Ackermann, M. {et~al.} 2013, Astrophys. J. Suppl., 209, 34

\bibitem[{Ackermann {et~al.}(2014)}]{Fermi-LAT:2014sfa}
Ackermann, M. {et~al.} 2014, Astrophys. J., 793, 64

\bibitem[{Ackermann {et~al.}(2015)}]{Ackermann:2014usa}
Ackermann, M. {et~al.} 2015, Astrophys.J., 799, 86

\bibitem[{Ackermann {et~al.}(2016)}]{TheFermi-LAT:2015ykq}
Ackermann, M. {et~al.} 2016, Phys. Rev. Lett., 116, 151105

\bibitem[{Ajello {et~al.}(2015)Ajello, Gasparrini, S\'anchez-Conde, Zaharijas,
  Gustafsson, {et~al.}}]{Ajello:2015mfa}
Ajello, M., Gasparrini, D., S\'anchez-Conde, M., {et~al.} 2015, Astrophys.J.,
  800, L27

\bibitem[{Ajello {et~al.}(2012)Ajello, Shaw, Romani, Dermer, Costamante,
  {et~al.}}]{Ajello:2011zi}
Ajello, M., Shaw, M., Romani, R., {et~al.} 2012, \apj, 751, 108

\bibitem[{Ajello {et~al.}(2017{\natexlab{a}})}]{TheFermi-LAT:2017pvy}
Ajello, M. {et~al.} 2017{\natexlab{a}}, Astrophys. J. Suppl., 232, 18

\bibitem[{Ajello {et~al.}(2017{\natexlab{b}})}]{Fermi-LAT:2017yoi}
Ajello, M. {et~al.} 2017{\natexlab{b}}, Submitted to: Astrophys. J.

\bibitem[{Atwood {et~al.}(2013)Atwood, Baldini, Bregeon, Bruel, Chekhtman,
  {et~al.}}]{Atwood:2013dra}
Atwood, W., Baldini, L., Bregeon, J., {et~al.} 2013, Astrophys.J., 774, 76

\bibitem[{Bechtol {et~al.}(2017)Bechtol, Ahlers, Di~Mauro, Ajello, \&
  Vandenbroucke}]{Bechtol:2015uqb}
Bechtol, K., Ahlers, M., Di~Mauro, M., Ajello, M., \& Vandenbroucke, J. 2017,
  Astrophys. J., 836, 47

\bibitem[{{Casandjian} {et~al.}(2009){Casandjian}, {Grenier}, \& {for the Fermi
  Large Area Telescope Collaboration}}]{2009arXiv0912.3478C}
{Casandjian}, J.-M., {Grenier}, I., \& {for the Fermi Large Area Telescope
  Collaboration}. 2009, arXiv:0912.3478

\bibitem[{{Damiani} {et~al.}(1997){Damiani}, {Maggio}, {Micela}, \&
  {Sciortino}}]{1997ApJ...483..350D}
{Damiani}, F., {Maggio}, A., {Micela}, G., \& {Sciortino}, S. 1997, \apj, 483,
  350

\bibitem[{Di~Mauro {et~al.}(2014{\natexlab{a}})Di~Mauro, Calore, Donato,
  Ajello, \& Latronico}]{DiMauro:2013xta}
Di~Mauro, M., Calore, F., Donato, F., Ajello, M., \& Latronico, L.
  2014{\natexlab{a}}, \apj, 780, 161

\bibitem[{Di~Mauro {et~al.}(2014{\natexlab{b}})Di~Mauro, Cuoco, Donato, \&
  Siegal-Gaskins}]{DiMauro:2014wha}
Di~Mauro, M., Cuoco, A., Donato, F., \& Siegal-Gaskins, J.~M.
  2014{\natexlab{b}}, JCAP, 1411, 021

\bibitem[{Di~Mauro \& Donato(2015)}]{DiMauro:2015tfa}
Di~Mauro, M. \& Donato, F. 2015, Phys.Rev., D91, 123001

\bibitem[{Di~Mauro {et~al.}(2014{\natexlab{c}})Di~Mauro, Donato, Lamanna,
  Sanchez, \& Serpico}]{DiMauro:2013zfa}
Di~Mauro, M., Donato, F., Lamanna, G., Sanchez, D., \& Serpico, P.
  2014{\natexlab{c}}, Astrophys.J., 786, 129

\bibitem[{Eddington(1913)}]{eddington1913}
Eddington, A.~S. 1913, \mnras, 359, 73

\bibitem[{Fornasa \& Sánchez-Conde(2015)}]{Fornasa:2015qua}
Fornasa, M. \& Sánchez-Conde, M.~A. 2015, Phys. Rept., 598, 1

\bibitem[{Gavish \& Eichler(2016)}]{Gavish:2016tfl}
Gavish, E. \& Eichler, D. 2016, Astrophys. J., 822, 56

\bibitem[{{G{\'o}rski} {et~al.}(2005){G{\'o}rski}, {Hivon}, {Banday},
  {Wandelt}, {Hansen}, {Reinecke}, \& {Bartelmann}}]{2005ApJ...622..759G}
{G{\'o}rski}, K.~M., {Hivon}, E., {Banday}, A.~J., {et~al.} 2005, \apj, 622,
  759

\bibitem[{Lamastra {et~al.}(2017)Lamastra, Menci, Fiore, Antonelli,
  Colafrancesco, Guetta, \& Stamerra}]{Lamastra:2017iyo}
Lamastra, A., Menci, N., Fiore, F., {et~al.} 2017, Astron. Astrophys., 607, A18

\bibitem[{Lisanti {et~al.}(2016)Lisanti, Mishra-Sharma, Necib, \&
  Safdi}]{Lisanti:2016jub}
Lisanti, M., Mishra-Sharma, S., Necib, L., \& Safdi, B.~R. 2016, Astrophys. J.,
  832, 117

\bibitem[{Malyshev \& Hogg(2011)}]{Malyshev:2011zi}
Malyshev, D. \& Hogg, D.~W. 2011, Astrophys. J., 738, 181

\bibitem[{{Starck} \& {Pierre}(1998)}]{1998A&AS..128..397S}
{Starck}, J.-L. \& {Pierre}, M. 1998, \aaps, 128, 397

\bibitem[{{Su} {et~al.}(2010){Su}, {Slatyer}, \&
  {Finkbeiner}}]{2010ApJ...724.1044S}
{Su}, M., {Slatyer}, T.~R., \& {Finkbeiner}, D.~P. 2010, \apj, 724, 1044

\bibitem[{Wilks(1938)}]{wilks1938}
Wilks, S.~S. 1938, Ann. Math. Statist., 9, 60

\bibitem[{{Wood} {et~al.}(2017){Wood}, {Caputo}, {Charles}, {Di Mauro},
  {Magill}, \& {Jeremy Perkins for the Fermi-LAT
  Collaboration}}]{2017arXiv170709551W}
{Wood}, M., {Caputo}, R., {Charles}, E., {et~al.} 2017, ArXiv:1707.09551

\bibitem[{Zechlin {et~al.}(2016{\natexlab{a}})Zechlin, Cuoco, Donato, Fornengo,
  \& Regis}]{Zechlin:2016pme}
Zechlin, H.-S., Cuoco, A., Donato, F., Fornengo, N., \& Regis, M.
  2016{\natexlab{a}}, Astrophys. J., 826, L31

\bibitem[{Zechlin {et~al.}(2016{\natexlab{b}})Zechlin, Cuoco, Donato, Fornengo,
  \& Vittino}]{Zechlin:2015wdz}
Zechlin, H.-S., Cuoco, A., Donato, F., Fornengo, N., \& Vittino, A.
  2016{\natexlab{b}}, Astrophys. J. Suppl., 225, 18

\end{thebibliography}
