from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import matplotlib.pyplot as pl
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf
import pyfits as fits
from catalog_simdet import *


import os.path
newpath='match_sim_det_bestOff'
if not os.path.exists(newpath):
    os.makedirs(newpath)

#user= 'silvia' #'mattia'
user= 'silvia'

if (user=='silvia'):
    path='/home/silvia/Dropbox/3FHLanalysis/efficiency'
elif (user=='mattia'):
    path='/Users/mattiadimauro/Dropbox/FERMI-LAT/3FHLanalysis/efficiency'


filesim= path+'/sim_best_Off/sim1/files/catalog_sim_prova.fits'
filesimdet=path+'/sim_best_Off/catalogs_bestOff/catalog_bfOff_sim1_02deg_4.fits'


def ratio_err(num, den, err_num, err_den):
  rel_num= np.divide(err_num,num)
  rel_den= np.divide(err_den,den)
  return np.sqrt(np.power(rel_num, 2) + np.power(-rel_den, 2))*(num/den)




catsim= catalog(filesim, True, 1)
catdet= catalog(filesimdet, False, 1)

catsim()
catdet()

catdet_posr68= catdet.read_field('pos_r68')

glat_cut=20
ts_cut=25.
sources=0.
match=0.
nomatch=0.
doublematch=0.
spurious = 0
vere=0
R2=[]

namesource_sp= []
ra_sp=[]
dec_sp=[]
glon_sp=[]
glat_sp=[]
gamma_sp=[]
gamma_err_sp=[]
flux_sp= []
flux_err_sp=[]
posr68_sp=[]
ts_sp=[]
npred_sp=[]
#a1 = np.array(namesource)
#a2 = np.array(ra_vec)
#a3 = np.array(dec_vec)
#a4 = np.array(glon_vec)
#a5 = np.array(glat_vec)
#a6 = np.array(Gamma_vec)
#a7 = np.array(flux_vec)
ts_vere=[]
npred_vere=[]
flux_vere=[]

flux_sim_match=[]
flux_cat_match=[]
flux_cat_err_match=[]
index_cat_match=[]
index_cat_err_match=[]
index_sim_match=[]

#Detected->Simulated
for j in range(len(catdet.source_name)):
  
  
  if(np.abs(catdet.GLAT[j])>=glat_cut and catdet.TS[j]>= ts_cut):# and catdet.TS[j]<= 25):
    ts_vere.append(catdet.TS[j])
    flux_vere.append(catdet.flux[j])
    local_match= 0
    sources= sources+1
    if(catdet_posr68[j]>0.):
	catdet_posr68[j]=catdet_posr68[j]
    else:
	catdet_posr68[j]=0.012 
    
    Avec = ( np.power( catsim.GLON -catdet.GLON[j], 2) + np.power( catsim.GLAT -catdet.GLAT[j], 2) )/np.power( catdet_posr68[j], 2 )
    Amin= np.min( Avec )
    #print 'Amin', Amin
    ind=[i[0] for i in sorted(enumerate(Avec), key=lambda x:x[1])]
    #print 'INDICE', Avec[ind[0]]
    #ind=[i[0] for i in sorted(enumerate(Avec), key=lambda x:x[1])]
    if(Amin>16.):
        spurious = spurious + 1
        print catdet.GLON[j], catdet.GLAT[j],catsim.GLON[ind[0]],catsim.GLAT[ind[0]],catdet_posr68[j],( np.power( catsim.GLON[ind[0]] -catdet.GLON[j], 2) + np.power( catsim.GLAT[ind[0]] -catdet.GLAT[j], 2) )/np.power( catdet_posr68[j], 2 )
        namesource_sp.append(catdet.source_name[j])
	glon_sp.append(catdet.GLON[j])
	glat_sp.append(catdet.GLAT[j])
	gamma_sp.append(catdet.param_index[j])
	gamma_err_sp.append(catdet.param_index_err[j])
	flux_sp.append(catdet.flux[j])
	flux_err_sp.append(catdet.flux_err[j])
	ts_sp.append(catdet.TS[j])
	posr68_sp.append(catdet_posr68[j])
	npred_sp.append(catdet.read_field('Npred')[j])
    elif(Amin<=16. and catdet.flux_err[j]>0.):
      vere= vere +1
      
      npred_vere.append(catdet.read_field('Npred')[j])
      #if(catdet.TS[j]>= ts_cut):
	#namesource_sp.append(catdet.source_name[j])
        #glon_sp.append(catdet.GLON[j])
        #glat_sp.append(catdet.GLAT[j])
        #gamma_sp.append(catdet.param_index[j])
        #gamma_err_sp.append(catdet.param_index_err[j])
        #flux_sp.append(catdet.flux[j])
        #flux_err_sp.append(catdet.flux_err[j])
        #ts_sp.append(catdet.TS[j])
        #posr68_sp.append(catdet_posr68[j])
        #npred_sp.append(catdet.read_field('Npred')[j])
   
      #print np.min((np.power( catsim.GLON -catdet.GLON[j], 2) + np.power( catsim.GLAT -catdet.GLAT[j], 2))/np.power( catdet_posr68[j], 2) )
      
      _R= np.power( (catsim.param_index-np.abs(catdet.param_index[j]))/( catdet.param_index_err[j]), 2) + np.power( catsim.flux -catdet.flux[j], 2)/ np.power( catdet.flux_err[j], 2) + (np.power( catsim.GLON -catdet.GLON[j], 2) + np.power( catsim.GLAT -catdet.GLAT[j], 2))/np.power( catdet_posr68[j], 2)
      
      #_R=  np.power( catsim.flux -catdet.flux[j], 2)/ np.power( catdet.flux_err[j], 2) + (np.power( catsim.GLON -catdet.GLON[j], 2) + np.power( catsim.GLAT -catdet.GLAT[j], 2))/np.power( catdet_posr68[j], 2)
      
      ind=[i[0] for i in sorted(enumerate(_R), key=lambda x:x[1])]
      
      #print ind[0], 
      R2.append(np.min(_R)) 
      
      #double match
      if(len(np.where(_R<48.)[0])>1):
	print np.abs(catdet.param_index[j]), ( catdet.param_index_err[j]), catdet.flux[j],  catdet.flux_err[j] ,catdet.GLON[j],   catdet.GLAT[j], catdet_posr68[j]
	doublematch=doublematch+1

	
	
	
      #print 'double match', len(np.where(_R<27.)[0])
      
      #print 'check',  _R[ind[0]], np.min(_R)
      #print  catsim.param_index[ind[0]], np.abs(catdet.param_index[j]),  catdet.param_index_err[j], catsim.flux[ind[0]], catdet.flux[j],  catdet.flux_err[j] , catsim.GLON[ind[0]], catdet.GLON[j],  catsim.GLAT[ind[0]], catdet.GLAT[j], catdet_posr68[j]
      flux_cat_match.append(catdet.flux[j])
      flux_sim_match.append(catsim.flux[ind[0]])
      index_cat_match.append(np.abs(catdet.param_index[j]))
      index_sim_match.append(catsim.param_index[ind[0]])


#catdet_GLAT20= catdet.sort_field(16. ,20., catdet.GLAT)
#print 'sources', (len(catdet_GLAT20))
      
   
print 'TS CUT', ts_cut, 'number of matches', len(R2) , 'among ',sources, 'sources',  sources/np.float(len(R2))
print 'Spurious', spurious
R2=np.array(R2)
x= np.where(R2>48.)[0]
print 'R2 higher than 48 for',  len(x),
print 'double match', doublematch


Rmin=0.
Rmax=40.
Rnbin=10.

Rbins= np.linspace(Rmin, Rmax, Rnbin)
Rval=np.zeros(len(Rbins)-1)
for j in range(len(Rbins)-1):
  Rval[j]=(Rbins[j+1]+Rbins[j])/2.

deltaR= np.zeros(Rnbin-1)
for i in range(len(deltaR)):
  deltaR[i]= Rbins[i+1]-Rbins[i]
  
  
R2h= np.histogram(R2, Rbins)


fig = plt.figure(figsize=(8,6))
plt.errorbar(Rval, R2h[0]/np.float(len(R2)), xerr=deltaR/2., fmt='o', color='blue', label='TS 16')
plt.xlabel(r'$R^2$', fontsize=18)
plt.ylabel(r'$N/N_{\rm{det}}$', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.legend(loc=1,prop={'size':18},numpoints=1, scatterpoints=1)

plt.savefig(newpath+'/R2match_4sigmapos_LP'+str(ts_cut)+'.pdf')
#plt.show()



'''
ISO LOW
TS CUT 25 number of matches 928 among  938.0 sources 1.01077586207
spurious 10
R2 higher than 48 for 21 double match 0.0

TS CUT 16 number of matches 1359 among  1701.0 sources 1.25165562914
Spurious 342
R2 higher than 48 for 32 double match 40.0

ISO AV
TS CUT 25 number of matches 928 among  938.0 sources 1.01077586207
R2 higher than 48 for 21 double match 0.0

TS CUT 16 number of matches 1359 among  1701.0 sources 1.25165562914
R2 higher than 48 for 32 double match 40.0



ISO HIGH
TS CUT 16 number of matches 1290 among  1670.0 sources 1.29457364341
Spurious 380
R2 higher than 48 for 30 double match 38.0

TS CUT 25 number of matches 892 among  901.0 sources 1.0100896861
spurious 9
R2 higher than 48 for 23 double match 0.0
'''




##CREATE CATALOG

a1 = np.array(namesource_sp)
a4 = np.array(glon_sp)
a5 = np.array(glat_sp)
a6 = np.array(gamma_sp)
a7 = np.array(gamma_err_sp)
a8 = np.array(flux_sp)
a9 = np.array(flux_err_sp)
a10= np.array(ts_sp)
a11=np.array(npred_sp)
a12= np.array(posr68_sp)
col1 = fits.Column(name='Source_Name', unit='', format='20A', array=a1)
#col2 = fits.Column(name='RAJ2000', unit='deg', format='E', array=a2)
#col3 = fits.Column(name='DEJ2000', unit='deg', format='E', array=a3)
col4 = fits.Column(name='GLON', unit='deg', format='E', array=a4)
col5 = fits.Column(name='GLAT', unit='deg', format='E', array=a5)
col6 = fits.Column(name='Index', unit='', format='E', array=a6)
col7 = fits.Column(name='Index err', unit='', format='E', array=a7)
col8 = fits.Column(name='Flux', unit='ph/cm^2/s', format='E', array=a8)
col9 = fits.Column(name='Flux err', unit='ph/cm^2/s', format='E', array=a9)
col10 = fits.Column(name='ts', unit='', format='E', array=a10)
col11=fits.Column(name='npred', unit='', format='E', array=a11)
col12=fits.Column(name='pos68', unit='', format='E', array=a12)
cols = fits.ColDefs([col1, col4, col5, col6, col7, col8, col9, col10, col11, col12])
tbhdu = fits.BinTableHDU.from_columns(cols)
#tbhdu.writeto('catalog_spurie_sim30'+str(ts_cut)+'.fits')



#hist Nspurie/Ndet
tsnbin=20
tsbins= np.linspace(12, 40, tsnbin)
tsval=np.zeros(len(tsbins)-1)
for j in range(len(tsbins)-1):
  tsval[j]=(tsbins[j+1]+tsbins[j])/2.

deltats= np.zeros(tsnbin-1)
for i in range(len(deltats)):
  deltats[i]= tsbins[i+1]-tsbins[i]
  
  
tsvere= np.histogram(ts_vere, tsbins)
tsspurie= np.histogram(ts_sp, tsbins)


tsvereh= tsvere[0]
tsverehf= tsvereh.astype(np.float)

print tsspurie[0], tsverehf
fig = plt.figure(figsize=(8,6))
plt.errorbar(tsval,tsspurie[0]/tsverehf , xerr=deltats/2., yerr= ratio_err(tsspurie[0], tsverehf, np.sqrt(tsspurie[0]), np.sqrt( tsverehf)), fmt='o', color='blue')
plt.xlabel(r'$TS$', fontsize=18)
plt.ylabel(r'$N_{\rm{spur}}/N_{\rm{det}}$', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.legend(loc=1,prop={'size':18},numpoints=1, scatterpoints=1)
plt.savefig(newpath+'/Nsp_allTS'+str(ts_cut)+'.pdf')
#plt.show()




#hist Nspurie/Ndet

Snbins=9
Smin = 5.9e-12
Smax=8e-10
#Log bins for the flux
Sbins= 10**np.linspace(np.log10(Smin), np.log10(Smax), Snbins) 
Sbins= np.concatenate((Sbins, [1.3e-9,5.e-9]), axis=0)
Snbins= Snbins +2
#Compute centers of the bins
Sval = np.zeros(len(Sbins)-1) 
for j in range(len(Sbins)-2):
  #Sval[j]= 10.**((np.log10(Sbins[j+1])+ np.log10(Sbins[j]))/2.)
  Sval[j]= (Sbins[j+1]+Sbins[j])/2.
Sval[len(Sbins)-2]= (Sbins[len(Sbins)-2]+Sbins[len(Sbins)-1])/2.
#Compute DeltaS
deltaS=np.zeros(Snbins-1)
for i in range(len(deltaS)):
  deltaS[i]= Sbins[i+1]-Sbins[i]



#fluxnbin=20
#fluxbins= np.logspace(np.log10(1e-12), np.log10(1e-8), fluxnbin)
#fluxval=np.zeros(len(fluxbins)-1)
#for j in range(len(fluxbins)-1):
  #fluxval[j]=(fluxbins[j+1]+fluxbins[j])/2.

#deltaflux= np.zeros(fluxnbin-1)
#for i in range(len(deltaflux)):
  #deltaflux[i]= fluxbins[i+1]-fluxbins[i]
  
  
fluxvere= np.histogram(flux_vere, Sbins)
fluxspurie= np.histogram(flux_sp, Sbins)


fluxvereh= fluxvere[0]
fluxverehf= fluxvereh.astype(np.float)

print fluxspurie[0], fluxverehf
fig = plt.figure(figsize=(8,6))
plt.errorbar(Sval,fluxspurie[0]/fluxverehf , xerr=deltaS/2., yerr=ratio_err(fluxspurie[0], fluxverehf, np.sqrt(fluxspurie[0]), np.sqrt( fluxverehf)),fmt='o', color='blue')
plt.xlabel(r'$S$ [photon/cm$^2$/s]', fontsize=22)
plt.ylabel(r'$N_{\rm SPUR}/N_{\rm TOT}$', fontsize=22)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.axis([1e-12,2e-9,1e-3, 1.1])
plt.yscale('log',nonposy='clip')
plt.xscale('log')
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.legend(loc=1,prop={'size':18},numpoints=1, scatterpoints=1)
plt.savefig(newpath+'/Nsp_allFLUX_TS'+str(ts_cut)+'.pdf')
#plt.show()

  
#INDEX MATCH
fig = pl.figure(figsize=(8,6))
plt.xlabel(r'$\Gamma_{\rm{CAT}}$', fontsize=22 )
plt.ylabel(r'$\Gamma_{\rm{SIM}}$', fontsize=22)
plt.errorbar(index_cat_match, index_sim_match,  color='black',fmt='o') 
plt.plot( index_sim_match,  index_sim_match,color='blue')
plt.axis([1,5,1,5], fontsize=18)
pl.grid(True)
#pl.yscale('log')
#pl.xscale('log')
pl.xticks(fontsize=22)
pl.yticks(fontsize=22)
pl.tick_params('both', length=10, width=2, which='major')
pl.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.savefig(newpath+'/Gamma_match'+str(ts_cut)+'.pdf')  
  
  
#FLUX MATCH
fig = pl.figure(figsize=(8,6))
plt.xlabel(r'$S_{\rm{CAT}}$', fontsize=22)
plt.ylabel(r'$S_{\rm{SIM}}$', fontsize=22)
plt.errorbar(flux_cat_match, flux_sim_match,  color='black',fmt='o') 
plt.plot(flux_sim_match,flux_sim_match, ls='--', lw=2.0, color='blue')
plt.axis([1e-11,1e-8,1e-11,1e-8], fontsize=18)
pl.grid(True)
pl.yscale('log')
pl.xscale('log')
pl.xticks(fontsize=22)
pl.yticks(fontsize=22)
pl.tick_params('both', length=10, width=2, which='major')
pl.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)

fig.tight_layout(pad=0.5)  
plt.savefig(newpath+'/flux_match_TS'+str(ts_cut)+'.pdf')
plt.savefig('../plot/flux_match_TS'+str(ts_cut)+'.pdf')



#FLUX MATCH #2
fig = pl.figure(figsize=(8,6))
plt.xlabel(r'$S$ [photon cm$^{-2}$ s$^{-1}$]', fontsize=22)
plt.ylabel(r'$S_{\rm{SIM}}/S$', fontsize=22)
plt.errorbar(flux_cat_match, np.divide(flux_sim_match, flux_cat_match),  color='black',fmt='.', ms=10) 
#plt.plot(flux_sim_match,flux_sim_match, ls='--', lw=2.0, color='blue')
plt.axis([1e-11,1e-8,3e-1,10], fontsize=18)
pl.grid(True)
pl.yscale('log')
pl.xscale('log')
pl.xticks(fontsize=22)
pl.yticks(fontsize=22)
pl.tick_params('both', length=10, width=2, which='major')
pl.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)

fig.tight_layout(pad=0.5)  
plt.savefig(newpath+'/flux_match_TS'+str(ts_cut)+'_paper.pdf')