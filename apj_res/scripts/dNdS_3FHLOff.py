from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf

pigreco= np.pi
solidangle=27143.6175

def dNdS_BPL(SS,indexup,indexdown,Sbreak,normbpl):
    if SS>breakF:
        return normbpl*np.power(SS,-indexup)
    if SS<=breakF:
        return normbpl*np.power(SS,-indexdown)*np.power(breakF,-indexup+indexdown)

indexup = 2.18#2.6 #index above the break
indexdown = 1.50#1.73 #index below the break
norm = np.power(10., -13.90305)#1.1e-18#1/((ph/cm^2/s))/deg^2
breakF = np.power(10., -10.1738)#1.6e-10

Sval_fit = np.power(10.,np.arange(-15.,-6,0.1))
dNdS_fit = np.zeros(len(Sval_fit))
for h in range(len(Sval_fit)):
  dNdS_fit[h] = dNdS_BPL(Sval_fit[h], indexup, indexdown, breakF,norm)

#---------------------------------------
#Plot dNdS
#---------------------------------------
#was this problem http://stackoverflow.com/questions/13491829/missing-errobars-when-using-yscalelog-at-matplotlib

Sval16=np.array([  9.75000000e-12,   1.60000000e-11,   2.20321097e-11,
         2.90640857e-11,   3.83404536e-11,   5.05775546e-11,
         6.67203642e-11,   8.80154653e-11,   1.16107312e-10,
         1.53165217e-10,   2.02050873e-10,   2.66539337e-10,
         3.51610547e-10,   6.00000000e-10,   1.40000000e-09,
         4.00000000e-09])
deltaS16=np.array([  6.50000000e-12,   6.00000000e-12,   6.06421931e-12,
         7.99973277e-12,   1.05530030e-11,   1.39211990e-11,
         1.83644203e-11,   2.42257819e-11,   3.19579111e-11,
         4.21578996e-11,   5.56134127e-11,   7.33635144e-11,
         9.67789060e-11,   4.00000000e-10,   1.20000000e-09,
         4.00000000e-09])
dNdS_simcorr_err16=np.array([  1.01259934e+09,   1.58142554e+08,   8.13102654e+07,
         6.11079799e+07,   4.09150246e+07,   3.00117698e+07,
         2.33997362e+07,   1.50878237e+07,   8.37665925e+06,
         6.38469457e+06,   3.89995724e+06,   2.41042367e+06,
         1.79861791e+06,   4.64730827e+05,   1.11372757e+05,
         2.07833944e+04])
dNdS_simcorr16=np.array([  4.08312448e+09,   1.76468741e+09,   1.00969079e+09,
         7.11977522e+08,   4.16133297e+08,   2.85811294e+08,
         2.00611142e+08,   1.16792698e+08,   5.32593502e+07,
         3.39066617e+07,   1.73561749e+07,   9.03908875e+06,
         6.31916438e+06,   1.74995098e+06,   2.88588407e+05,
         3.49990195e+04])
dNdS_sim_err16=np.array([  2.50926744e+07,   7.85367042e+07,   9.15717550e+07,
         6.47366562e+07,   4.27849647e+07,   3.02663119e+07,
         2.03993026e+07,   1.40369989e+07,   8.83980228e+06,
         5.49921754e+06,   3.50535286e+06,   2.24577966e+06,
         1.58791111e+06,   4.11895720e+05,   9.41271596e+04,
         1.84205366e+04])
dNdS_sim16=np.array([  1.11090005e+08,   1.00453326e+09,   1.38027525e+09,
         9.10004905e+08,   5.24355883e+08,   3.46149234e+08,
         2.07431921e+08,   1.29566899e+08,   6.77846276e+07,
         3.46057681e+07,   1.85485839e+07,   1.00434319e+07,
         6.62370242e+06,   1.84205366e+06,   2.88588407e+05,
         3.68410732e+04])
dNdS_catcorr_err16=np.array([  7.62411990e+08,   1.10874300e+08,   7.09047315e+07,
         6.28110030e+07,   4.61113664e+07,   2.83032451e+07,
         2.37284286e+07,   1.28152645e+07,   8.08003372e+06,
         5.74019292e+06,   3.44257728e+06,   2.38825215e+06,
         1.53384012e+06,   4.19003692e+05,   1.15914963e+05,
         1.73317697e+04])
dNdS_catcorr16=np.array([  2.58299843e+09,   1.22818955e+09,   9.19583910e+08,
         7.70496227e+08,   5.01102922e+08,   2.64087932e+08,
         2.06342888e+08,   9.12862459e+07,   5.07656013e+07,
         2.88206624e+07,   1.44634790e+07,   8.94959282e+06,
         4.97012929e+06,   1.48745833e+06,   3.07008943e+05,
         2.62492646e+04])

'''        
R_TS16 = 1.0-np.array([ 0.79888268,  0.52606429,  0.17405643,  0.01953943,  0.01809955,
        0.01583113,  0.01470588,  0.        ,  0.        ,  0.        ])
errR_TS16 = np.array([ 0.06960173,  0.01867468,  0.00865342,  0.00372851,  0.00456565,
        0.00651399,  0.00855265,  0.        ,  0.        ,  0.        ])
        
R_TS20 = 1.0-np.array([ 0.75757576,  0.27591707,  0.09445209,  0.01953943,  0.01809955,
        0.01583113,  0.01470588,  0.        ,  0.        ,  0.        ])
errR_TS20 = np.array([ 0.13086908,  0.02369555,  0.00688456,  0.00372851,  0.00456565,
        0.00651399,  0.00855265,  0.        ,  0.        ,  0.        ])

R_TS25 = 1.0-np.array([ 0.09756098,0.09756098,  0.03789894,  0.01958042,  0.01809955,
        0.01583113,  0.01470588,  0.        ,  0.        ,  0.        ])
errR_TS25 = np.array([ 0.02950529,0.02950529,  0.00511408,  0.0037364 ,  0.00456565,
        0.00651399,  0.00855265,  0.        ,  0.        ,  0.        ])
        
dNdS_catcorr_err16 = dNdS_catcorr16*np.sqrt( np.power(dNdS_catcorr_err16/dNdS_catcorr16,2.) + np.power(errR_TS16/R_TS16,2.))
#dNdS_catcorr_err20 = dNdS_catcorr20*np.sqrt( np.power(dNdS_catcorr_err20/dNdS_catcorr20,2.) + np.power(errR_TS20/R_TS20,2.))
#dNdS_catcorr_err25 = dNdS_catcorr25*np.sqrt( np.power(dNdS_catcorr_err25/dNdS_catcorr25,2.) + np.power(errR_TS25/R_TS25,2.))
''' 
         
fig = plt.figure(figsize=(8,6))
plt.errorbar(Sval16, dNdS_sim16*Sval16*Sval16, xerr= deltaS16/2., yerr= dNdS_sim_err16*Sval16*Sval16, color='red',fmt='*', label='SIM det')
plt.errorbar(Sval16*1.05, dNdS_simcorr16*Sval16*Sval16, xerr= deltaS16/2., yerr= dNdS_simcorr_err16*Sval16*Sval16, color='blue', fmt='d', label=r'SIM corr')
plt.errorbar(Sval16*0.95, dNdS_catcorr16*Sval16*Sval16, xerr= deltaS16/2., yerr= dNdS_catcorr_err16*Sval16*Sval16, color='black', fmt='o', label=r'CAT corr')
plt.plot(Sval_fit, dNdS_fit*Sval_fit*Sval_fit,color='black',lw=2.0,linestyle='--', label='Input')
#plt.plot(Sval_line, dNdS_corr*Sval_line*Sval_line,color='black',lw=2.0,label='Input')
plt.xscale('log')
plt.yscale('log', nonposy='clip')

plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
plt.savefig("../plot/dNdS_3FHL_corr_TS16_OFF.pdf")


#---------------------------------------
#Plot dNdS
#---------------------------------------
#was this problem http://stackoverflow.com/questions/13491829/missing-errobars-when-using-yscalelog-at-matplotlib

Sval25=np.array([  9.75000000e-12,   1.60000000e-11,   2.20321097e-11,
         2.90640857e-11,   3.83404536e-11,   5.05775546e-11,
         6.67203642e-11,   8.80154653e-11,   1.16107312e-10,
         1.53165217e-10,   2.02050873e-10,   2.66539337e-10,
         3.51610547e-10,   6.00000000e-10,   1.40000000e-09,
         4.00000000e-09])
deltaS25=np.array([  6.50000000e-12,   6.00000000e-12,   6.06421931e-12,
         7.99973277e-12,   1.05530030e-11,   1.39211990e-11,
         1.83644203e-11,   2.42257819e-11,   3.19579111e-11,
         4.21578996e-11,   5.56134127e-11,   7.33635144e-11,
         9.67789060e-11,   4.00000000e-10,   1.20000000e-09,
         4.00000000e-09])
dNdS_simcorr_err25=np.array([  3.78085086e+01,   5.15104172e+08,   1.29734430e+08,
         6.84023636e+07,   4.11655759e+07,   3.00117698e+07,
         2.33997362e+07,   1.50878237e+07,   8.37665925e+06,
         6.38469457e+06,   3.89995724e+06,   2.41042367e+06,
         1.79861791e+06,   4.64730827e+05,   1.11372757e+05,
         2.07833944e+04])
dNdS_simcorr25=np.array([  4.08312448e+01,   1.76468741e+09,   1.00969079e+09,
         7.11977522e+08,   4.16133297e+08,   2.85811294e+08,
         2.00611142e+08,   1.16792698e+08,   5.32593502e+07,
         3.39066617e+07,   1.73561749e+07,   9.03908875e+06,
         6.31916438e+06,   1.74995098e+06,   2.88588407e+05,
         3.49990195e+04])
dNdS_sim_err25=np.array([  6.70629933e+01,   2.31379669e+07,   5.37917168e+07,
         5.66281910e+07,   4.24704703e+07,   3.02663119e+07,
         2.03993026e+07,   1.40369989e+07,   8.83980228e+06,
         5.49921754e+06,   3.50535286e+06,   2.24577966e+06,
         1.58791111e+06,   4.11895720e+05,   9.41271596e+04,
         1.84205366e+04])
dNdS_sim25=np.array([  7.93500038e+01,   8.71905399e+07,   4.76292164e+08,
         6.96319543e+08,   5.16675570e+08,   3.46149234e+08,
         2.07431921e+08,   1.29566899e+08,   6.77846276e+07,
         3.46057681e+07,   1.85485839e+07,   1.00434319e+07,
         6.62370242e+06,   1.84205366e+06,   2.88588407e+05,
         3.68410732e+04])
dNdS_catcorr_err25=np.array([  2.25749764e+01,   4.83225620e+08,   1.24133583e+08,
         7.26425024e+07,   4.63217055e+07,   2.83032451e+07,
         2.35855143e+07,   1.28152645e+07,   8.08003372e+06,
         5.74019292e+06,   3.44257728e+06,   2.38825215e+06,
         1.53384012e+06,   4.19003692e+05,   1.15914963e+05,
         1.73317697e+04])
dNdS_catcorr25=np.array([  2.04156224e+01,   1.67533615e+09,   9.68528331e+08,
         7.92097756e+08,   5.00133642e+08,   2.64087932e+08,
         2.04432306e+08,   9.12862459e+07,   5.07656013e+07,
         2.88206624e+07,   1.44634790e+07,   8.94959282e+06,
         4.97012929e+06,   1.48745833e+06,   3.07008943e+05,
         2.62492646e+04])
         
#dNdS_catcorr_err16 = dNdS_catcorr16*np.sqrt( np.power(dNdS_catcorr_err16/dNdS_catcorr16,2.) + np.power(errR_TS16/R_TS16,2.))
#dNdS_catcorr_err20 = dNdS_catcorr20*np.sqrt( np.power(dNdS_catcorr_err20/dNdS_catcorr20,2.) + np.power(errR_TS20/R_TS20,2.))
#dNdS_catcorr_err25 = dNdS_catcorr25*np.sqrt( np.power(dNdS_catcorr_err25/dNdS_catcorr25,2.) + np.power(errR_TS25/R_TS25,2.))


fig = plt.figure(figsize=(8,6))
plt.errorbar(Sval25, dNdS_sim25*Sval25*Sval25, xerr= deltaS25/2., yerr= dNdS_sim_err25*Sval25*Sval25, color='red',fmt='*', label='SIM det')
plt.errorbar(Sval25*1.05, dNdS_simcorr25*Sval25*Sval25, xerr= deltaS25/2., yerr= dNdS_simcorr_err25*Sval25*Sval25, color='blue', fmt='d', label=r'SIM corr')
plt.errorbar(Sval25*0.95, dNdS_catcorr25*Sval25*Sval25, xerr= deltaS25/2., yerr= dNdS_catcorr_err25*Sval25*Sval25, color='black', fmt='o', label=r'CAT corr')
plt.plot(Sval_fit, dNdS_fit*Sval_fit*Sval_fit,color='black',lw=2.0,linestyle='--', label='Input')
#plt.plot(Sval_line, dNdS_corr*Sval_line*Sval_line,color='black',lw=2.0,label='Input')
plt.xscale('log')
plt.yscale('log', nonposy='clip')

plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
plt.savefig("../plot/dNdS_3FHL_corr_TS25_OFF.pdf")