from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf

pigreco= np.pi

from catalog import *
from eff_utils import *


#SET PARAMETERS
TS_cut= 25. #20. #16.
glat_cut= 20. 

#REFERENCE SIMULATION IS s...
#refsim=['sim1']
refsim= ['sim1', 'sim2', 'sim3', 'sim4','sim5']
path='sim1_nobias'


#DEF ANALYSIS CONSTANT 
solidangle= get_solidangle(glat_cut)
nsim= np.float(len(refsim))


#INPUT dNdS SIMULATION (to read from readme?)
indexup = 2.1840#2.6 #index above the break
indexdown = 1.55#1.73 #index below the break
norm = np.power(10., -13.94305)#1.1e-18#1/((ph/cm^2/s))/deg^2
breakF = np.power(10., -10.1738)#1.6e-10


#indexup = 2.25 #index above the break
#indexdown = 1.73 #index below the break
#norm = 2.6e-15#1/((ph/cm^2/s))/deg^2
#breakF = 1.e-10

Sval_fit = np.power(10.,np.arange(-15.,-6,0.01))
dNdS_ = np.zeros(len(Sval_fit))
for h in range(len(Sval_fit)):
  dNdS_[h] = dNdS_BPL(Sval_fit[h], norm, indexup, indexdown, breakF)
  


#---------------------------------------
#Plot dNdS
#---------------------------------------
#was this problem http://stackoverflow.com/questions/13491829/missing-errobars-when-using-yscalelog-at-matplotlib
plt.close()
fig = plt.figure(figsize=(8,6))
plt.errorbar(Sval, dNdS_sim*Sval*Sval/solidangle, xerr= deltaS/2., yerr= dNdS_sim_err*Sval*Sval/solidangle, color='red',fmt='*', label='SIM det')
plt.errorbar(Sval*1.02, dNdS_simcorr*Sval*Sval/solidangle, xerr= deltaS/2., yerr= dNdS_simcorr_err*Sval*Sval/solidangle, color='blue', fmt='d', label='SIM corr')
plt.errorbar(Sval*0.98, dNdS_catcorr*Sval*Sval/solidangle, xerr= deltaS/2., yerr= dNdS_catcorr_err*Sval*Sval/solidangle, color='black', fmt='o', label='CAT corr')
plt.plot(Sval_fit, dNdS_*Sval_fit*Sval_fit,color='black',lw=2.0,linestyle='--', label='Input')
#plt.plot(Sval_line, dNdS_corr*Sval_line*Sval_line,color='black',lw=2.0,label='Input')
plt.xscale('log')
plt.yscale('log', nonposy='clip')

plt.xlabel(r'$S$ [photon/cm$^2$/s]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph/cm$^2$/s/deg$^2$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,3e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
plt.savefig(str(path)+'/dNdS_3FHL_corr_'+str(refsim)+'_TS'+str(TS_cut)+'.pdf')
plt.show()







