from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf


pigreco= np.pi 


#EFFICIENCY

with open("../data/eff_results/sim_best_Off/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS16.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS16CB= [x[2] for x in data]
    Werr_TS16CB=[x[3] for x in data]
    Ndet_TS16=[x[4] for x in data]
    
with open("../data/eff_results/sim_best_Off/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS20.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS20= [x[0] for x in data]
    Serr_TS20= [x[1] for x in data]
    Wval_TS20CB= [x[2] for x in data]
    Werr_TS20CB=[x[3] for x in data]  
    Ndet_TS20=[x[4] for x in data]


with open("../data/eff_results/sim_best_Off/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS25= [x[0] for x in data]
    Serr_TS25= [x[1] for x in data]
    Wval_TS25CB= [x[2] for x in data]
    Werr_TS25CB=[x[3] for x in data]
    Ndet_TS25=[x[4] for x in data]

with open("../data/eff_results/sim_best_Off/sim1_nobias/eff_TS25.0_binomial_SB_trick.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS25nobias= [x[0] for x in data]
    Serr_TS25nobias= [x[1] for x in data]
    Wval_TS25CBnobias= [x[2] for x in data]
    Werr_TS25CBnobias=[x[3] for x in data]
  
with open("../data/eff_results/sim_best_Off/simall/nspurius_simbestoffall_TS16.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    nspur_TS16=[x[1] for x in data]


with open("../data/eff_results/sim_best_Off/simall/nspurius_simbestoffall_TS20.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    nspur_TS20=[x[1] for x in data]
    
    
with open("../data/eff_results/sim_best_Off/simall/nspurius_simbestoffall_TS25.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    nspur_TS25=[x[1] for x in data]    
    
ones= np.ones(len(Sval))  


#COMPUTE CORRECTION FACTOR FOR SPURIUS SOURCES

R_TS16= np.divide(nspur_TS16, Ndet_TS16)
R_TS20= np.divide(nspur_TS20, Ndet_TS20)
R_TS25= np.divide(nspur_TS25, Ndet_TS25)





    
    
fig = plt.figure(figsize=(8,6))


plt.errorbar(np.multiply(Sval, 1.08),np.multiply(Wval_TS16CB, np.subtract(ones,R_TS16)), xerr=Serr, yerr=np.multiply(Werr_TS16CB, np.subtract(ones, R_TS16)), fmt='d', color='k',label='$\omega$: $TS>16$ ')
plt.errorbar(np.multiply(Sval_TS20, 1.),np.multiply(Wval_TS20CB, np.subtract(ones, R_TS20)), xerr=Serr_TS20, yerr=np.multiply(Werr_TS20CB,np.subtract(ones,R_TS20)), fmt='h', color='dodgerblue', label='$\omega$: $TS>20$')
plt.errorbar(np.multiply(Sval_TS25, 0.92),np.multiply(Wval_TS25CB, np.subtract(ones, R_TS25)), xerr=Serr_TS25, yerr=np.multiply(Werr_TS25CB,np.subtract(ones, R_TS25)), fmt='o', color='r', label='$\omega$: $TS>25$')
plt.plot(Sval_TS25nobias, Wval_TS25CBnobias, 'r--', label=r'$\tilde{\omega}$: $TS>25$')

plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.ylabel(r'$\omega$, $\tilde{\omega}$', fontsize=22)
plt.xlabel(r'$S$ [photon/cm$^2$/s]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
plt.axis([1e-12,2e-8,5e-3,3], fontsize=18)
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
plt.savefig('../plot/eff_bfOffall_TS162025_obsintr.pdf')
plt.show()
plt.close()     
    
    
  