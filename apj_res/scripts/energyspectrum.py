#---------------------------------------
#script which create xml file
#---------------------------------------
import astropy.io.fits as fits
from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
import random
from math import *
import matplotlib.pyplot as pl
import numpy as np
from astropy import units as uni
from astropy.coordinates import SkyCoord
import os.path

Enspectrum_real=np.array([ 16155.,  12749.,   9807.,   7291.,   5444.,   4332.,   3295.,
         2512.,   1779.,   1413.,   1022.,    843.,    582.,    479.,
          345.,    239.,    195.,    147.,    141.,    107.,     78.,
           84.,     61.,     43.])
Enspectrum_tot=np.array([ 16739.32967668,  12937.0739181 ,   9652.84842962,   7249.05963199,
         5299.11173947,   4195.67434874,   3212.88067485,   2332.67507334,
         1772.95802912,   1265.49557816,    965.50095202,    829.83638727,
          659.17664356,    452.80675681,    337.975608  ,    301.92499662,
          236.74804632,    185.15132204,    159.50982498,    131.22376486,
          104.44906605,     96.01470921,     76.74387041,     73.69897128])
Enspectrum_sources=np.array([ 2882.,  2140.,  1569.,  1199.,   879.,   790.,   594.,   440.,
         356.,   275.,   213.,   170.,   167.,   112.,    99.,    92.,
          67.,    55.,    41.,    40.,    29.,    32.,    19.,    29.])
Enspectrum_iso=np.array([ 7391.46779255,  6176.23109036,  4907.4525842 ,  3851.21881501,
        2827.43462397,  2240.09725923,  1766.33341805,  1312.03932378,
         972.94123198,   671.16015506,   515.40217988,   469.161531  ,
         357.21048634,   250.93811786,   169.002933  ,   147.91070719,
         127.62972084,    95.99138213,    97.61386104,    71.65419851,
          65.16428288,    49.75073325,    40.82709925,    38.39338089])
Enspectrum_diff=np.array([  6.46486188e+03,   4.61984283e+03,   3.17539585e+03,
         2.19784082e+03,   1.59167712e+03,   1.16457709e+03,
         8.51547257e+02,   5.79635750e+02,   4.43016797e+02,
         3.18335423e+02,   2.36098772e+02,   1.89674856e+02,
         1.33966157e+02,   8.88686389e+01,   6.89726750e+01,
         6.10142894e+01,   4.11183255e+01,   3.31599399e+01,
         1.98959639e+01,   1.85695663e+01,   9.28478317e+00,
         1.32639760e+01,   1.59167712e+01,   5.30559039e+00])
Eval=np.array([  11006.94190046,   13335.2146964 ,   16155.98111699,
         19573.41735065,   23713.73648989,   28729.84797391,
         34807.00532803,   42169.64991743,   51089.69732431,
         61896.58210053,   74989.42038762,   90851.75617455,
        110069.41809604,  133352.14132071,  161559.80384936,
        195734.17675622,  237137.37513713,  287298.49141   ,
        348070.06571993,  421696.5035771 ,  510896.96960899,
        618965.82387065,  749894.21268171,  908517.5617455 ])
deltaE =np.array([   2115.277,    2562.716,    3104.801,    3761.552,    4557.226,
          5521.204,    6689.092,    8104.02 ,    9818.244,   11895.076,
         14411.208,   17459.584,   21152.768,   25627.152,   31048.016,
         37615.536,   45572.256,   55212.048,   66890.912,   81040.192,
         98182.432,  118950.784,  144112.064,  174595.84 ])

fig = pl.figure(figsize=(8,6))
pl.errorbar(Eval, Enspectrum_real*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_real)*np.power(Eval,1.), fmt="o", color="black", label='Real')
pl.errorbar(Eval, Enspectrum_sources*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_sources)*np.power(Eval,1.), fmt=">", color="brown", label='PS')
pl.errorbar(Eval, Enspectrum_iso*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_iso)*np.power(Eval,1.), fmt="x", color="red", label='Iso')
pl.errorbar(Eval, Enspectrum_diff*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_diff)*np.power(Eval,1.), fmt="*", color="green", label='Diff')
pl.errorbar(Eval*0.98, Enspectrum_tot*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_tot)*np.power(Eval,1.), fmt="o", color="blue", label='Tot')
#pl.errorbar(Eval*1.02, Enspectrum_tot*np.power(Eval,1.)/par0, xerr = deltaE/2., yerr=np.sqrt(Enspectrum_tot/par0)*np.power(Eval,1.), fmt="o", color="red", label='Tot norm=1')
#pl.errorbar(flux/1e4, 1.34961e-14*np.power(flux/1e4,-2.50)/(solidangle), fmt="-", color="black",label=r'SIM')
pl.legend(loc=1,prop={'size':13},numpoints=1, scatterpoints=1)
#pl.title(r'$S^2dN/dS$ for Pass8 catatalog $E=[3,10]$GeV', fontsize=18)
pl.ylabel(r'$EdN/dE$', fontsize=18)
pl.xlabel(r'$E$ [MeV]', fontsize=18)
pl.axis([1e4,1e6,5e06,3e8], fontsize=18)
pl.xticks(fontsize=20)
pl.yticks(fontsize=20)
pl.grid(True)
pl.yscale('log')
pl.xscale('log')
fig.tight_layout(pad=0.5)
pl.savefig("../plot/Enspectrum_bestfitOff_binbybin.pdf")
#pl.savefig("Enspectrum_Off_overall.png")

#f, (ax1, ax2) = pl.subplots(2, sharex=True)
#ax1.plot(x, y)
#ax1.set_title('Sharing Y axis')
#ax2.scatter(x, y)

resierr = np.sqrt( np.power(np.sqrt(Enspectrum_tot)/Enspectrum_real,2.) + np.power(np.sqrt(Enspectrum_real)*Enspectrum_tot/np.power(Enspectrum_real,2.),2.))


fig = pl.figure(1,figsize=(8,6))

pl.subplot(211)
#ax1 = pl.subplot(311)
pl.errorbar(Eval, Enspectrum_real*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_real)*np.power(Eval,1.), fmt="o", color="black", label='Real')
pl.errorbar(Eval, Enspectrum_sources*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_sources)*np.power(Eval,1.), fmt=">", color="brown", label='PS')
pl.errorbar(Eval, Enspectrum_iso*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_iso)*np.power(Eval,1.), fmt="x", color="red", label='Iso')
pl.errorbar(Eval, Enspectrum_diff*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_diff)*np.power(Eval,1.), fmt="*", color="green", label='Diff')
pl.errorbar(Eval*1, Enspectrum_tot*np.power(Eval,1.), xerr = deltaE/2., yerr=np.sqrt(Enspectrum_tot)*np.power(Eval,1.), fmt="o", color="blue", label='Tot')
#pl.setp(ax1.get_xticklabels(), fontsize=6)
#pl.setp(ax1.get_xticklabels(), visible=False)
pl.ylabel(r'$EdN/dE$', fontsize=18)
#pl.xlabel(r'$E$ [MeV]', fontsize=18)
pl.axis([1e4,1e6,5e06,3e8], fontsize=18)
pl.legend(loc=1,ncol=3,prop={'size':13},numpoints=1)
#pl.legend(loc=9,ncol=1,prop={'size':13},numpoints=1, scatterpoints=1)
pl.xticks(fontsize=18)
pl.yticks(fontsize=18)
pl.yscale('log')
pl.xscale('log')
fig.tight_layout(pad=0.5)

#ax2 = pl.subplot(312, sharex=ax1)
fig = pl.figure(1,figsize=(8,3))

pl.subplot(212)
pl.errorbar(Eval, Enspectrum_tot/Enspectrum_real, xerr = deltaE/2., yerr=resierr, fmt="o", color="black")
# make these tick labels invisible
#pl.errorbar(Eval*1.02, Enspectrum_tot*np.power(Eval,1.)/par0, xerr = deltaE/2., yerr=np.sqrt(Enspectrum_tot/par0)*np.power(Eval,1.), fmt="o", color="red", label='Tot norm=1')
#pl.errorbar(flux/1e4, 1.34961e-14*np.power(flux/1e4,-2.50)/(solidangle), fmt="-", color="black",label=r'SIM')
#pl.legend(loc=1,prop={'size':13},numpoints=1, scatterpoints=1)
#pl.title(r'$S^2dN/dS$ for Pass8 catatalog $E=[3,10]$GeV', fontsize=18)
pl.ylabel(r'SIM/REAL', fontsize=18)
pl.xlabel(r'$E$ [MeV]', fontsize=18)
pl.axis([1e4,1e6,0.6,2.0], fontsize=18)
pl.xticks(fontsize=18)
pl.yticks(fontsize=18)
pl.grid(True)
pl.yscale('linear')
pl.xscale('log')

fig.subplots_adjust(hspace=0)
pl.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)

fig.tight_layout(pad=0.5)
pl.savefig("../plot/Enspectrum_bestfitOff_binbybin_2panel.pdf")
#pl.savefig("Enspectrum_Off_overall.png")