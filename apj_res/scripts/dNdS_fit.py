from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf
from iminuit import Minuit
from scipy import stats

pigreco= np.pi

#BPL
def dNdS_PL(SS,norm,index):
        return norm*np.power(SS,-index)
        
def dNdSint_PL(SS,norm,index):
    SS = np.power(10.,SS)
    return norm*np.power(SS,-index)*np.log(10.)*SS

def dNdS_BPL(SS,norm,indexup,indexdown,breakF):
    if SS>breakF:
        return norm*np.power(SS,-indexup)
    if SS<=breakF:
        return norm*np.power(SS,-indexdown)*np.power(breakF,-indexup+indexdown)
        
def dNdSint_BPL(SS,norm,indexup,indexdown,breakF):
    SS = np.power(10.,SS)
    if SS>breakF:
        return norm*np.power(SS,-indexup)*np.log(10.)*SS
    if SS<=breakF:
        return norm*np.power(SS,-indexdown)*np.power(breakF,-indexup+indexdown)*np.log(10.)*SS
        
def dNdS_DBPL(SS,norm,index1,index2,index3,breakF1,breakF2):
    if SS>breakF1:
        return norm*np.power(SS,-index3)
    if SS>breakF2 and SS<=breakF1:
        return norm*np.power(SS,-index2)*np.power(breakF1,-index3+index2)
    if SS<=breakF2:
        return norm*np.power(SS,-index1)*np.power(breakF1,-index3+index2)*np.power(breakF2,-index2+index1)
        
def dNdSint_DBPL(SS,norm,index1,index2,index3,breakF1,breakF2):
    SS = np.power(10.,SS)
    if SS>breakF1:
        return norm*np.power(SS,-index3)*np.log(10.)*SS
    if SS>breakF2 and SS<=breakF1:
        return norm*np.power(SS,-index2)*np.power(breakF1,-index3+index2)*np.log(10.)*SS
    if SS<=breakF2:
        return norm*np.power(SS,-index1)*np.power(breakF1,-index3+index2)*np.power(breakF2,-index2+index1)*np.log(10.)*SS

def dNdS_LP(SS,norm,alpha,beta,S0):
    return norm*np.power(SS/S0,-alpha+beta*log(SS/S0))

def dNdSint_LP(SS,norm,alpha,beta,S0):
    SS=np.power(10.,SS) 
    return norm*np.power(SS/S0,-alpha+beta*log(SS/S0))*log(10.)*SS
      
def functheta(theta):
    return np.cos(theta)
    
solidangle20deg = 4.*(pigreco*180./180.)*romberg(functheta, pigreco*20./180.,pigreco*90./180.)*np.power(180./np.pi,2.) 

'''
with open("model_prediction.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[0:]]
    Sval_model= [x[0] for x in data]
    S2dNdS_band= [x[1] for x in data]

Sval_model= Sval_model[0:31]
S2dNdS_down= S2dNdS_band[0:31]
S2dNdS_up= S2dNdS_band[31:62]
S2dNdS_up=S2dNdS_up[::-1]
'''
#print Sval_model, S2dNdS_band, S2dNdS_down, S2dNdS_up
#print len(Sval_model), len(S2dNdS_down), len(S2dNdS_up)

    
#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/dNdS_catcorr_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS16.0.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS16= [x[0] for x in data]
    Serr_TS16= [x[1] for x in data]
    dNdS_catcorr_TS16= [x[2] for x in data]
    dNdS_catcorr_err_TS16=[x[3] for x in data] 

Sval_TS16 = np.array(Sval_TS16)
Serr_TS16 = np.array(Serr_TS16)
dNdS_catcorr_TS16= np.array(dNdS_catcorr_TS16)
dNdS_catcorr_err_TS16=np.array(dNdS_catcorr_err_TS16)

#with open("dNdS_catcorr_['sim30']_TS20.0.dat") as source_file:
with open("../data/dNdS_catcorr_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS20.0.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS20= [x[0] for x in data]
    Serr_TS20= [x[1] for x in data]
    dNdS_catcorr_TS20= [x[2] for x in data]
    dNdS_catcorr_err_TS20=[x[3] for x in data] 
    
Sval_TS20 = np.array(Sval_TS20)
Serr_TS20 = np.array(Serr_TS20)
dNdS_catcorr_TS20= np.array(dNdS_catcorr_TS20)
dNdS_catcorr_err_TS20=np.array(dNdS_catcorr_err_TS20)

#with open("dNdS_catcorr_['sim30']_TS25.0.dat") as source_file:
with open("../data/dNdS_catcorr_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS25= [x[0] for x in data]
    Serr_TS25= [x[1] for x in data]
    dNdS_catcorr_TS25= [x[2] for x in data]
    dNdS_catcorr_err_TS25=[x[3] for x in data] 
    
Sval_TS25 = np.array(Sval_TS25)
Serr_TS25 = np.array(Serr_TS25)
dNdS_catcorr_TS25= np.array(dNdS_catcorr_TS25)
dNdS_catcorr_err_TS25=np.array(dNdS_catcorr_err_TS25)

#Input dNdS in the simulation
#normfit, indexupfit, indexdownfit, Sbreakfit)
indexup = 2.1570397692 #index above the break
indexdown = 1.25855650002 #index below the break
norm = 2.0553048605440387e-14#1/((ph/cm^2/s))/deg^2
breakF = 4.5696258213861013e-11

Svalfit = np.power(10.,np.arange(-13.,-8.,0.005))
dNdS_fit = np.zeros(len(Svalfit))
for h in range(len(Svalfit)):
  dNdS_fit[h] = dNdS_BPL(Svalfit[h], norm, indexup, indexdown, breakF)


###BANDA
Svalband = np.array([  1.00000000e-13,   1.04712855e-13,   1.09647820e-13,
          1.14815362e-13,   1.20226443e-13,   1.25892541e-13,
          1.31825674e-13,   1.38038426e-13,   1.44543977e-13,
          1.51356125e-13,   1.58489319e-13,   1.65958691e-13,
          1.73780083e-13,   1.81970086e-13,   1.90546072e-13,
          1.99526231e-13,   2.08929613e-13,   2.18776162e-13,
          2.29086765e-13,   2.39883292e-13,   2.51188643e-13,
          2.63026799e-13,   2.75422870e-13,   2.88403150e-13,
          3.01995172e-13,   3.16227766e-13,   3.31131121e-13,
          3.46736850e-13,   3.63078055e-13,   3.80189396e-13,
          3.98107171e-13,   4.16869383e-13,   4.36515832e-13,
          4.57088190e-13,   4.78630092e-13,   5.01187234e-13,
          5.24807460e-13,   5.49540874e-13,   5.75439937e-13,
          6.02559586e-13,   6.30957344e-13,   6.60693448e-13,
          6.91830971e-13,   7.24435960e-13,   7.58577575e-13,
          7.94328235e-13,   8.31763771e-13,   8.70963590e-13,
          9.12010839e-13,   9.54992586e-13,   1.00000000e-12,
          1.04712855e-12,   1.09647820e-12,   1.14815362e-12,
          1.20226443e-12,   1.25892541e-12,   1.31825674e-12,
          1.38038426e-12,   1.44543977e-12,   1.51356125e-12,
          1.58489319e-12,   1.65958691e-12,   1.73780083e-12,
          1.81970086e-12,   1.90546072e-12,   1.99526231e-12,
          2.08929613e-12,   2.18776162e-12,   2.29086765e-12,
          2.39883292e-12,   2.51188643e-12,   2.63026799e-12,
          2.75422870e-12,   2.88403150e-12,   3.01995172e-12,
          3.16227766e-12,   3.31131121e-12,   3.46736850e-12,
          3.63078055e-12,   3.80189396e-12,   3.98107171e-12,
          4.16869383e-12,   4.36515832e-12,   4.57088190e-12,
          4.78630092e-12,   5.01187234e-12,   5.24807460e-12,
          5.49540874e-12,   5.75439937e-12,   6.02559586e-12,
          6.30957344e-12,   6.60693448e-12,   6.91830971e-12,
          7.24435960e-12,   7.58577575e-12,   7.94328235e-12,
          8.31763771e-12,   8.70963590e-12,   9.12010839e-12,
          9.54992586e-12,   1.00000000e-11,   1.04712855e-11,
          1.09647820e-11,   1.14815362e-11,   1.20226443e-11,
          1.25892541e-11,   1.31825674e-11,   1.38038426e-11,
          1.44543977e-11,   1.51356125e-11,   1.58489319e-11,
          1.65958691e-11,   1.73780083e-11,   1.81970086e-11,
          1.90546072e-11,   1.99526231e-11,   2.08929613e-11,
          2.18776162e-11,   2.29086765e-11,   2.39883292e-11,
          2.51188643e-11,   2.63026799e-11,   2.75422870e-11,
          2.88403150e-11,   3.01995172e-11,   3.16227766e-11,
          3.31131121e-11,   3.46736850e-11,   3.63078055e-11,
          3.80189396e-11,   3.98107171e-11,   4.16869383e-11,
          4.36515832e-11,   4.57088190e-11,   4.78630092e-11,
          5.01187234e-11,   5.24807460e-11,   5.49540874e-11,
          5.75439937e-11,   6.02559586e-11,   6.30957344e-11,
          6.60693448e-11,   6.91830971e-11,   7.24435960e-11,
          7.58577575e-11,   7.94328235e-11,   8.31763771e-11,
          8.70963590e-11,   9.12010839e-11,   9.54992586e-11,
          1.00000000e-10,   1.04712855e-10,   1.09647820e-10,
          1.14815362e-10,   1.20226443e-10,   1.25892541e-10,
          1.31825674e-10,   1.38038426e-10,   1.44543977e-10,
          1.51356125e-10,   1.58489319e-10,   1.65958691e-10,
          1.73780083e-10,   1.81970086e-10,   1.90546072e-10,
          1.99526231e-10,   2.08929613e-10,   2.18776162e-10,
          2.29086765e-10,   2.39883292e-10,   2.51188643e-10,
          2.63026799e-10,   2.75422870e-10,   2.88403150e-10,
          3.01995172e-10,   3.16227766e-10,   3.31131121e-10,
          3.46736850e-10,   3.63078055e-10,   3.80189396e-10,
          3.98107171e-10,   4.16869383e-10,   4.36515832e-10,
          4.57088190e-10,   4.78630092e-10,   5.01187234e-10,
          5.24807460e-10,   5.49540874e-10,   5.75439937e-10,
          6.02559586e-10,   6.30957344e-10,   6.60693448e-10,
          6.91830971e-10,   7.24435960e-10,   7.58577575e-10,
          7.94328235e-10,   8.31763771e-10,   8.70963590e-10,
          9.12010839e-10,   9.54992586e-10,   1.00000000e-09,
          1.04712855e-09,   1.09647820e-09,   1.14815362e-09,
          1.20226443e-09,   1.25892541e-09,   1.31825674e-09,
          1.38038426e-09,   1.44543977e-09,   1.51356125e-09,
          1.58489319e-09,   1.65958691e-09,   1.73780083e-09,
          1.81970086e-09,   1.90546072e-09,   1.99526231e-09,
          2.08929613e-09,   2.18776162e-09,   2.29086765e-09,
          2.39883292e-09,   2.51188643e-09,   2.63026799e-09,
          2.75422870e-09,   2.88403150e-09,   3.01995172e-09,
          3.16227766e-09,   3.31131121e-09,   3.46736850e-09,
          3.63078055e-09,   3.80189396e-09,   3.98107171e-09,
          4.16869383e-09,   4.36515832e-09,   4.57088190e-09,
          4.78630092e-09,   5.01187234e-09,   5.24807460e-09,
          5.49540874e-09,   5.75439937e-09,   6.02559586e-09,
          6.30957344e-09,   6.60693448e-09,   6.91830971e-09,
          7.24435960e-09,   7.58577575e-09,   7.94328235e-09,
          8.31763771e-09,   8.70963590e-09,   9.12010839e-09,
          9.54992586e-09,   1.00000000e-08,   1.04712855e-08,
          1.09647820e-08,   1.14815362e-08,   1.20226443e-08,
          1.25892541e-08,   1.31825674e-08,   1.38038426e-08,
          1.44543977e-08,   1.51356125e-08,   1.58489319e-08,
          1.65958691e-08,   1.73780083e-08,   1.81970086e-08,
          1.90546072e-08,   1.99526231e-08,   2.08929613e-08,
          2.18776162e-08,   2.29086765e-08,   2.39883292e-08,
          2.51188643e-08,   2.63026799e-08,   2.75422870e-08,
          2.88403150e-08,   3.01995172e-08,   3.16227766e-08,
          3.31131121e-08,   3.46736850e-08,   3.63078055e-08,
          3.80189396e-08,   3.98107171e-08,   4.16869383e-08,
          4.36515832e-08,   4.57088190e-08,   4.78630092e-08,
          5.01187234e-08,   5.24807460e-08,   5.49540874e-08,
          5.75439937e-08,   6.02559586e-08,   6.30957344e-08,
          6.60693448e-08,   6.91830971e-08,   7.24435960e-08,
          7.58577575e-08,   7.94328235e-08,   8.31763771e-08,
          8.70963590e-08,   9.12010839e-08,   9.54992586e-08])
dNdS_down = np.array([  3.59938382e-15,   3.75429297e-15,   3.91586905e-15,
          4.08439899e-15,   4.26018207e-15,   4.44353044e-15,
          4.63476971e-15,   4.83423947e-15,   5.04229395e-15,
          5.25930261e-15,   5.48565082e-15,   5.72174053e-15,
          5.96799100e-15,   6.22483952e-15,   6.49274221e-15,
          6.77217481e-15,   7.06363354e-15,   7.36763597e-15,
          7.68472197e-15,   8.01545462e-15,   8.36042124e-15,
          8.72023442e-15,   9.09553312e-15,   9.48698382e-15,
          9.89528164e-15,   1.03211517e-14,   1.07653501e-14,
          1.12286659e-14,   1.17119217e-14,   1.22159757e-14,
          1.27417230e-14,   1.32900973e-14,   1.38620723e-14,
          1.44586638e-14,   1.50809312e-14,   1.57299796e-14,
          1.64069615e-14,   1.71130792e-14,   1.78495865e-14,
          1.86177914e-14,   1.94190580e-14,   2.02548094e-14,
          2.11265295e-14,   2.20357664e-14,   2.29841348e-14,
          2.39733187e-14,   2.50050749e-14,   2.60812355e-14,
          2.72037115e-14,   2.83744962e-14,   2.95956689e-14,
          3.08693980e-14,   3.21979454e-14,   3.35836705e-14,
          3.50290339e-14,   3.65366025e-14,   3.81090533e-14,
          3.97491787e-14,   4.14598914e-14,   4.32442291e-14,
          4.51053607e-14,   4.70465910e-14,   4.90713674e-14,
          5.11832855e-14,   5.33860956e-14,   5.56837096e-14,
          5.80802076e-14,   6.05798453e-14,   6.31870616e-14,
          6.59064865e-14,   6.87429491e-14,   7.17014866e-14,
          7.47873526e-14,   7.80060271e-14,   8.13632260e-14,
          8.48649109e-14,   8.85173003e-14,   9.23268800e-14,
          9.63004152e-14,   1.00444962e-13,   1.04767881e-13,
          1.09276848e-13,   1.13979871e-13,   1.18885300e-13,
          1.24001849e-13,   1.29338601e-13,   1.34905035e-13,
          1.40711036e-13,   1.46766913e-13,   1.53083421e-13,
          1.59671777e-13,   1.66543681e-13,   1.73711336e-13,
          1.81187470e-13,   1.88985359e-13,   1.97118851e-13,
          2.05602390e-13,   2.14451042e-13,   2.23680518e-13,
          2.33307210e-13,   2.43348212e-13,   2.53821356e-13,
          2.64745240e-13,   2.76139262e-13,   2.88023657e-13,
          3.00419528e-13,   3.13348889e-13,   3.26834700e-13,
          3.40489294e-13,   3.54652855e-13,   3.69405588e-13,
          3.84772000e-13,   4.00777619e-13,   4.17449034e-13,
          4.34813942e-13,   4.51233441e-13,   4.68059730e-13,
          4.83711038e-13,   4.99673133e-13,   5.16161964e-13,
          5.32648872e-13,   5.49457241e-13,   5.66022769e-13,
          5.82477704e-13,   5.98796573e-13,   6.15572636e-13,
          6.32528124e-13,   6.49800200e-13,   6.67543913e-13,
          6.85772144e-13,   7.04498123e-13,   7.23735441e-13,
          7.43498061e-13,   7.63800328e-13,   7.84656976e-13,
          7.84403229e-13,   7.80836964e-13,   7.77286914e-13,
          7.73753004e-13,   7.70235160e-13,   7.66699280e-13,
          7.63162531e-13,   7.59642098e-13,   7.56137903e-13,
          7.52649874e-13,   7.49177934e-13,   7.45722010e-13,
          7.42282029e-13,   7.38857916e-13,   7.35449598e-13,
          7.32057002e-13,   7.28680057e-13,   7.25318689e-13,
          7.21972827e-13,   7.18642399e-13,   7.15327335e-13,
          7.12027562e-13,   7.08743012e-13,   7.05473613e-13,
          7.02219295e-13,   6.98979989e-13,   6.95628069e-13,
          6.92279014e-13,   6.88764935e-13,   6.85095741e-13,
          6.81446093e-13,   6.77634839e-13,   6.73204641e-13,
          6.67338850e-13,   6.61361908e-13,   6.55389886e-13,
          6.49471790e-13,   6.43607134e-13,   6.37662230e-13,
          6.31753070e-13,   6.25720079e-13,   6.18761727e-13,
          6.11880756e-13,   6.05076305e-13,   5.98347523e-13,
          5.91693569e-13,   5.85113610e-13,   5.78606825e-13,
          5.72172398e-13,   5.65809526e-13,   5.59517412e-13,
          5.53295270e-13,   5.47142322e-13,   5.41057798e-13,
          5.35040937e-13,   5.29090988e-13,   5.23207204e-13,
          5.17388852e-13,   5.11635203e-13,   5.05945538e-13,
          5.00319145e-13,   4.94755320e-13,   4.89253368e-13,
          4.83812601e-13,   4.78432339e-13,   4.73111908e-13,
          4.67850643e-13,   4.62647886e-13,   4.57502986e-13,
          4.52415301e-13,   4.47384194e-13,   4.42409035e-13,
          4.37489203e-13,   4.32624082e-13,   4.27813064e-13,
          4.23055547e-13,   4.18350936e-13,   4.13698643e-13,
          4.09098087e-13,   4.04548690e-13,   4.00049886e-13,
          3.95601111e-13,   3.91201809e-13,   3.86851429e-13,
          3.82549428e-13,   3.78295268e-13,   3.74088416e-13,
          3.69928347e-13,   3.65814540e-13,   3.61746481e-13,
          3.57723660e-13,   3.53745576e-13,   3.49811731e-13,
          3.45921631e-13,   3.42074792e-13,   3.38270732e-13,
          3.34508975e-13,   3.30789051e-13,   3.27110495e-13,
          3.23472846e-13,   3.19875650e-13,   3.16318456e-13,
          3.12800821e-13,   3.09322303e-13,   3.05882469e-13,
          3.02480887e-13,   2.99117133e-13,   2.95790786e-13,
          2.92501430e-13,   2.89248653e-13,   2.86032048e-13,
          2.82851214e-13,   2.79705753e-13,   2.76595271e-13,
          2.73519379e-13,   2.70477693e-13,   2.67469832e-13,
          2.64495420e-13,   2.61554085e-13,   2.58645460e-13,
          2.55769180e-13,   2.52924886e-13,   2.50112222e-13,
          2.47330836e-13,   2.44580381e-13,   2.41860512e-13,
          2.39170890e-13,   2.36511178e-13,   2.33881043e-13,
          2.31280157e-13,   2.28708195e-13,   2.26164834e-13,
          2.23649756e-13,   2.21162648e-13,   2.18703197e-13,
          2.16271097e-13,   2.13866043e-13,   2.11487735e-13,
          2.09135875e-13,   2.06810169e-13,   2.04510326e-13,
          2.02236058e-13,   1.99987082e-13,   1.97763115e-13,
          1.95563881e-13,   1.93389103e-13,   1.91238509e-13,
          1.89111832e-13,   1.87008804e-13,   1.84929163e-13,
          1.82872649e-13,   1.80839004e-13,   1.78827975e-13,
          1.76839309e-13,   1.74872759e-13,   1.72928077e-13,
          1.71005022e-13,   1.69103352e-13,   1.67222829e-13,
          1.65363219e-13,   1.63524289e-13,   1.61705809e-13,
          1.59907552e-13,   1.58129291e-13,   1.56370807e-13])
dNdS_up = np.array([  2.13861008e-14,   2.19700785e-14,   2.25700026e-14,
          2.31863084e-14,   2.38194434e-14,   2.44698670e-14,
          2.51380513e-14,   2.58244814e-14,   2.65296554e-14,
          2.72540852e-14,   2.79982965e-14,   2.87628296e-14,
          2.95482394e-14,   3.03550959e-14,   3.11839847e-14,
          3.20355075e-14,   3.29102824e-14,   3.38089442e-14,
          3.47321453e-14,   3.56805556e-14,   3.66548637e-14,
          3.76557766e-14,   3.86840209e-14,   3.97403428e-14,
          4.08255091e-14,   4.19403074e-14,   4.30855468e-14,
          4.42620586e-14,   4.54706967e-14,   4.67123385e-14,
          4.79878849e-14,   4.92982620e-14,   5.06444208e-14,
          5.20273384e-14,   5.34480185e-14,   5.49074923e-14,
          5.64068190e-14,   5.79470870e-14,   5.95294142e-14,
          6.11549491e-14,   6.28248715e-14,   6.45403935e-14,
          6.63027603e-14,   6.81132509e-14,   6.99731796e-14,
          7.18838963e-14,   7.38467878e-14,   7.58632788e-14,
          7.79348330e-14,   8.00629538e-14,   8.22491861e-14,
          8.44951165e-14,   8.68023752e-14,   8.91726368e-14,
          9.16076219e-14,   9.41090976e-14,   9.66788796e-14,
          9.93188332e-14,   1.02030874e-13,   1.04816972e-13,
          1.07679148e-13,   1.10619479e-13,   1.13640100e-13,
          1.16743204e-13,   1.19931043e-13,   1.23205929e-13,
          1.26570242e-13,   1.30026421e-13,   1.33576976e-13,
          1.37224485e-13,   1.40971594e-13,   1.44821022e-13,
          1.48775565e-13,   1.52838093e-13,   1.57011553e-13,
          1.61298975e-13,   1.65703472e-13,   1.70228240e-13,
          1.74876563e-13,   1.79651815e-13,   1.84557462e-13,
          1.89597065e-13,   1.94774281e-13,   2.00092868e-13,
          2.05556687e-13,   2.11169703e-13,   2.16935990e-13,
          2.22859735e-13,   2.28945235e-13,   2.35196909e-13,
          2.41619293e-13,   2.48217049e-13,   2.54994967e-13,
          2.61957966e-13,   2.69111098e-13,   2.76459558e-13,
          2.84008677e-13,   2.91763936e-13,   2.99730963e-13,
          3.07915541e-13,   3.16323610e-13,   3.24961274e-13,
          3.33872693e-13,   3.43226570e-13,   3.52842508e-13,
          3.62727849e-13,   3.72890140e-13,   3.83337142e-13,
          3.94076829e-13,   4.05117403e-13,   4.16467293e-13,
          4.28135165e-13,   4.40384717e-13,   4.53348558e-13,
          4.66694023e-13,   4.81127579e-13,   4.96653796e-13,
          5.13226212e-13,   5.32208756e-13,   5.52436055e-13,
          5.74208769e-13,   5.97681510e-13,   6.22113779e-13,
          6.47544800e-13,   6.74015401e-13,   7.01568079e-13,
          7.31136343e-13,   7.62076153e-13,   7.94325257e-13,
          8.27939060e-13,   8.61446200e-13,   8.85169835e-13,
          9.05531288e-13,   9.16767295e-13,   9.16812806e-13,
          9.14884077e-13,   9.07019855e-13,   9.14854283e-13,
          9.32053865e-13,   9.35878430e-13,   9.25470946e-13,
          9.15179200e-13,   9.05001903e-13,   8.94937783e-13,
          8.84985582e-13,   8.75144055e-13,   8.65411971e-13,
          8.55788113e-13,   8.46271278e-13,   8.36860275e-13,
          8.27553928e-13,   8.18351072e-13,   8.09250557e-13,
          8.00251245e-13,   7.91352011e-13,   7.82551740e-13,
          7.73849334e-13,   7.65243703e-13,   7.56733772e-13,
          7.48318475e-13,   7.39996762e-13,   7.31767590e-13,
          7.23629931e-13,   7.15582768e-13,   7.07625093e-13,
          6.99755912e-13,   6.91974241e-13,   6.84279106e-13,
          6.76698637e-13,   6.72876328e-13,   6.69786313e-13,
          6.66710488e-13,   6.63648787e-13,   6.60601147e-13,
          6.57567502e-13,   6.54547789e-13,   6.51541943e-13,
          6.48549900e-13,   6.45571598e-13,   6.42606972e-13,
          6.39655961e-13,   6.36718502e-13,   6.33794532e-13,
          6.30883990e-13,   6.27986814e-13,   6.25102943e-13,
          6.22232315e-13,   6.19374869e-13,   6.16530546e-13,
          6.13699284e-13,   6.10881024e-13,   6.08075707e-13,
          6.05283272e-13,   6.02503661e-13,   5.99736814e-13,
          5.96982674e-13,   5.94241181e-13,   5.91512277e-13,
          5.88795906e-13,   5.86092009e-13,   5.83400529e-13,
          5.80721408e-13,   5.78054591e-13,   5.75400021e-13,
          5.72779942e-13,   5.70175817e-13,   5.67583530e-13,
          5.65003030e-13,   5.62434261e-13,   5.59877172e-13,
          5.57331708e-13,   5.54797817e-13,   5.52275446e-13,
          5.49764543e-13,   5.47265056e-13,   5.44776933e-13,
          5.42300122e-13,   5.39834572e-13,   5.37380231e-13,
          5.34937049e-13,   5.32504974e-13,   5.30083957e-13,
          5.27673947e-13,   5.25274894e-13,   5.22886749e-13,
          5.20509461e-13,   5.18142981e-13,   5.15787260e-13,
          5.13442250e-13,   5.11107901e-13,   5.08784165e-13,
          5.06470994e-13,   5.04168340e-13,   5.01876154e-13,
          4.99594390e-13,   4.97323000e-13,   4.95061937e-13,
          4.92811153e-13,   4.90570603e-13,   4.88340239e-13,
          4.86120016e-13,   4.83909887e-13,   4.81709806e-13,
          4.79519727e-13,   4.77339606e-13,   4.75169397e-13,
          4.73009054e-13,   4.70858533e-13,   4.68717790e-13,
          4.66586779e-13,   4.64465457e-13,   4.62353780e-13,
          4.60251703e-13,   4.58159183e-13,   4.56076177e-13,
          4.54002641e-13,   4.51938532e-13,   4.49883808e-13,
          4.47838426e-13,   4.45802342e-13,   4.43775516e-13,
          4.41757905e-13,   4.39749467e-13,   4.37750160e-13,
          4.35759942e-13,   4.33778774e-13,   4.31806612e-13,
          4.29843417e-13,   4.27889147e-13,   4.25943763e-13,
          4.24007223e-13,   4.22079488e-13,   4.20160517e-13,
          4.18250270e-13,   4.16348709e-13,   4.14455792e-13,
          4.12571482e-13,   4.10695739e-13,   4.08828524e-13,
          4.06969798e-13,   4.05119523e-13,   4.03277660e-13,
          4.01444170e-13,   3.99619017e-13,   3.97802162e-13,
          3.95993567e-13,   3.94193195e-13,   3.92401008e-13,
          3.90616969e-13,   3.88841042e-13,   3.87073188e-13,
          3.85313372e-13,   3.83561557e-13,   3.81817707e-13,
          3.80081784e-13,   3.78353755e-13,   3.76633581e-13,
          3.74921229e-13,   3.73216661e-13,   3.71519843e-13])


dNdS_catcorr_TS25[0]=1e-10
dNdS_catcorr_err_TS25[0]=1e-10
fig = plt.figure(figsize=(8,6))
plt.fill_between(Svalband,dNdS_down, dNdS_up, color="#DEB887", alpha=0.5)
plt.plot(Svalband,np.multiply(np.power(Svalband, 2),dNdS_down)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'$1\sigma$ band')
plt.errorbar(0.94*np.multiply(Sval_TS16, 1), np.multiply(dNdS_catcorr_TS16, np.power(Sval_TS16, 2))/solidangle20deg, xerr= Serr_TS16, yerr= np.multiply(dNdS_catcorr_err_TS16, np.power(Sval_TS16, 2))/solidangle20deg, color='black', fmt='d', label='$TS>16$')
plt.errorbar(np.multiply(Sval_TS20, 1), np.multiply(dNdS_catcorr_TS20, np.power(Sval_TS20, 2))/solidangle20deg, xerr= Serr_TS20, yerr= np.multiply(dNdS_catcorr_err_TS20, np.power(Sval_TS20, 2))/solidangle20deg, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(1.06*np.multiply(Sval_TS25, 1), np.multiply(dNdS_catcorr_TS25, np.power(Sval_TS25, 2))/solidangle20deg, xerr= Serr_TS25, yerr= np.multiply(dNdS_catcorr_err_TS25, np.power(Sval_TS25, 2))/solidangle20deg, color='red', fmt='*', label='$TS>25$')
plt.plot(Svalfit, dNdS_fit*Svalfit*Svalfit,color='black',lw=1.5,label='Best Fit')
#plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
#plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Fit LP')
#plt.errorbar(Sval_1FHLcorr, np.array(dNdS_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr),xerr= deltaS_1FHLcorr, yerr= np.array(dNdSerr_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr), color='orange', fmt='*', label='1FHL corr')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [photon cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [photon cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_Off_corrected_bandfit_BPL_ts16.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')








