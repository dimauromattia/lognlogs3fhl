from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import matplotlib.pyplot as pl
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf


import os.path
newpath='match_our_off'
if not os.path.exists(newpath):
    os.makedirs(newpath)

#Calcola errore sul rapporto
def ratio_err(num, den, err_num, err_den):
  rel_num= np.divide(err_num,num)
  rel_den= np.divide(err_den,den)
  return np.sqrt(np.power(rel_num, 2) + np.power(-rel_den, 2))*(np.divide(num,den))

#Filename of OUR catalog
filename= '../data/catalog_allsources_clean02deg_step4.fits' 
glat_cut=20. 

#Select the fields
hduls= pf.open(filename)
header= hduls[1].header
source_name= hduls[1].data.field(0) 		#SOURCE NAME
GLON= hduls[1].data.field(9)  			#GLON
GLAT= hduls[1].data.field(10) 			#GLAT
TS= hduls[1].data.field(11)			#TS
spectral_index_= hduls[1].data.field(75) 	#SPECTRAL INDEX
spectral_index_er= hduls[1].data.field(76)
flux10000  = hduls[1].data.field('flux')    	#FLUX 1000
flux10000_err = hduls[1].data.field('flux_err') 

#spectral_index = []
#spectral_index_err= []
#Select only value wt unc
#for i in range(len(GLAT)):
  #if flux10000[i][0]<1e-14: print 'qui', i
#  spectral_index.append(np.abs(spectral_index_[i][1]))
#  spectral_index_err.append(spectral_index_er[i][1])
  
  
#print flux10000 
#print GLAT, spectral_index
spectral_index_GLAT20= [] #np.zeros(len(spectral_index))
spectral_index_GLAT20_unc= [] #np.zeros(len(spectral_index))
flux10000_GLAT20= []
flux10000_GLAT20_unc= []
TS_GLAT20 = []
#flux10000_= []

##Select for GLAT> glat_cut and TS>25
for i in range(len(GLAT)):
  if np.abs(GLAT[i])>glat_cut:
    ##plt.plot(GLAT[i], spectral_index[i], 'ko')
    	spectral_index_GLAT20.append(spectral_index_[i][1])
	spectral_index_GLAT20_unc.append(spectral_index_er[i][1])
    	flux10000_GLAT20.append(flux10000[i])
	flux10000_GLAT20_unc.append(flux10000_err[i])
	TS_GLAT20.append(TS[i])

#Filename of the OFFICIAL catalog
filename_uff= '../data/gll_psc7year10GeV_v9r1.fit'  

glat_cut=20. 
#Select the fields    
hduls_uff= pf.open(filename_uff)
header_uff= hduls_uff[1].header
source_name_uff= hduls_uff[1].data.field(0) 
GLON_uff= hduls_uff[1].data.field(4)  		#GLON
GLAT_uff= hduls_uff[1].data.field(5)		#GLAT
TSuff= hduls_uff[1].data.field(11)			#TS
spectral_index_uff= hduls_uff[1].data.field(23)
spectral_index_err_uff= hduls_uff[1].data.field(24)
flux10000_uff = hduls_uff[1].data.field(17)
flux10000_uff_err = hduls_uff[1].data.field(18)

source_20 = 0
source_20_run = 0
for j in range(len(GLAT_uff)):
  if(np.abs(GLAT_uff[j])>glat_cut and TSuff[j]>=25.):
    source_20 = source_20 + 1

for j in range(len(GLAT)):
  if(np.abs(GLAT[j])>glat_cut and TS[j]>=25.):
    source_20_run = source_20_run + 1
    
print source_20, source_20_run    

#Select for GLAT> glat_cut and TS>25

sources=0.
match=0.
nomatch=0.
doublematch=0.
TSh=0.
TSl=0.
TS_diff=[]

Index_uff = []
Index_our = []
Indexerr_uff = []
Indexerr_our = []
Flux_uff = []
Flux_our = []
Fluxerr_uff = []
Fluxerr_our = []
TS_uff = []
TS_our = []

#Ufficial --> Our catalog
#print 'TS', 'TS_uff', 'GLON', 'GLON_uff', 'GLAT', 'GLAT_uff'
print 'sourcename uff','\t','GLAT uff', '\t', 'GLON uff', '\t','TS uff'
for j in range(len(GLAT_uff)):
  if(np.abs(GLAT_uff[j])>=glat_cut and TSuff[j]>=25.):
    local_match= 0
    sources= sources+1
    double_match=0.
    for i in range(len(GLAT)):
        if(np.abs(GLAT[i])>=glat_cut and TS[i]>=25.):
	  d= np.power( np.power( GLON[i]-GLON_uff[j] ,2.0) + np.power(GLAT[i]-GLAT_uff[j],2.0) ,0.5)
	  if(d<0.2) and TSuff[j]/TS[i]<2. and TSuff[j]/TS[i]>0.6:
	    local_match= 1.
	    match= match +1
	    double_match= double_match +1

	    Index_uff.append(spectral_index_uff[j])
	    Index_our.append(-spectral_index_[i][1])
	    Indexerr_uff.append(spectral_index_err_uff[j])
	    Indexerr_our.append(spectral_index_er[i][1])
	    Flux_uff.append(flux10000_uff[j])
	    Flux_our.append(flux10000[i])
	    Fluxerr_uff.append(flux10000_uff_err[j])
	    Fluxerr_our.append(flux10000_err[i])
	    TS_uff.append(TSuff[j])
	    TS_our.append(TS[i])

	    if TS[i]/TSuff[j]>1.7 or TS[i]/TSuff[j]<1/1.7:
		print  GLON[i],GLON_uff[j],GLAT[i],GLAT_uff[j],d,TSuff[j],TS[i],flux10000_uff[j],flux10000[i], spectral_index_uff[j],spectral_index_[i][1]

	    if(TSuff[j]>TS[i]): 
	      TSh= TSh+1
	      TS_diff.append(TSuff[j]-TS[i])
	    elif(TSuff[j]<TS[i]): TSl= TSl+1
	    
	    
	    #plt.errorbar(flux10000[i], flux10000_uff[j], xerr= flux10000_err[i], yerr=flux10000_uff_err[j],  color='black',fmt='.')  
	    #plt.errorbar(spectral_index[i], spectral_index_uff[j], xerr= spectral_index_err[i], yerr=spectral_index_err_uff[j],  color='black',fmt='.') 
	    #plt.plot(TS[i], TS_uff[j], 'ko') 
	    #print TS[i], TS_uff[j], GLON[i], GLON_uff[j], GLAT[i], GLAT_uff[j]
    if(double_match>=2.): 
	doublematch = doublematch + 1
	print 'DOUBLE MATCH', double_match , doublematch
	
	    
    if(local_match==0): 
      nomatch=nomatch+1
      print  source_name_uff[j], '\t', GLAT_uff[j], '\t', GLON_uff[j],'\t', TSuff[j]

print 'number of matches=' ,match, 'among', sources, 'sources', 'no match', nomatch, 'double match',doublematch
print 'TS uff is higher than TS in cases', TSh
print 'TS uff is lesser than TS in cases', TSl
print np.mean(TS_diff), np.std(TS_diff)


#FLUX MATCH
fig = pl.figure(figsize=(8,6))
plt.xlabel(r'$S_{\rm{our}}$ photon cm$^{-2}$ s$^{-1}$')
plt.ylabel(r'$S_{\rm{off}}$ photon cm$^{-2}$ s$^{-1}$')
plt.errorbar(Flux_our, Flux_uff, xerr= Fluxerr_our, yerr=Fluxerr_uff,  color='black',fmt='o') 
plt.plot(flux10000_uff, flux10000_uff, ls='--', lw=2.0, color='blue')
plt.axis([1e-11,1e-8,1e-11,1e-8], fontsize=18)
plt.grid(True)
plt.yscale('log')
plt.xscale('log')
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.savefig('flux_match_TS25.pdf')
#plt.show()
fig.tight_layout(pad=0.5)

#FLUX MATCH PLOT PAPER
ratio_S= np.divide(Flux_uff, Flux_our)
ratio_S_err= ratio_err(Flux_uff, Flux_our, Fluxerr_uff, Fluxerr_our)
fig = pl.figure(figsize=(8,6))
plt.grid(True,ls=":", color='0.65')
plt.xlabel(r'$S$ [photon cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.ylabel(r'$S_{\rm{3FHL}}/S$', fontsize=18)
#plt.errorbar(Flux_our, ratio_S, xerr= Fluxerr_our, yerr=ratio_S_err,  color='black',fmt='.') 
plt.errorbar(Flux_our, ratio_S,  color='black',fmt='.', ms=10.) 
#plt.plot(flux10000_uff, flux10000_uff, ls='--', lw=2.0, color='blue')
plt.axis([9e-12,1e-8,6e-1,1.3], fontsize=18)
#plt.grid(True,ls=":", color='0.65')
plt.yscale('linear')
plt.xscale('log')
#plt.xaxis.set_ticks([0.5,0.6,0.7,0.8,0.9,1.0,1.2,1.4,1.6,1.8,2.0])
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.savefig(newpath+'/flux_match_TS25def.pdf')
plt.savefig('../plot/flux_match_TS25def.pdf')
fig.tight_layout(pad=0.5)
#plt.show()



#plt.xscale('log')
#plt.yscale('log', nonposy='clip')

#plt.xlabel('S our [photon/cm$^2$/s]')
#plt.ylabel('S ufficial [photon/cm$^2$/s]')
#plt.axis([1e-11,1e-8,1e-11,1e-8], fontsize=18)
##plt.title('3FHL official catalog')

#plt.plot(flux10000_uff, flux10000_uff, 'r', label='uff')
#plt.savefig('flux_match_TS25_25.pdf')
#plt.legend(loc=2)
#plt.show()

#INDEX MATCH
fig = pl.figure(figsize=(8,6))
plt.xlabel(r'$\Gamma$', fontsize=18)
plt.ylabel(r'$\Gamma_{\rm{3FHL}}$', fontsize=18)
plt.errorbar(Index_our, Index_uff, xerr=Indexerr_our, yerr=Indexerr_uff,  color='black',fmt='o', ms=4.0)
plt.plot(spectral_index_uff, spectral_index_uff,color='blue')
plt.axis([1,6,1,6], fontsize=18)
plt.grid(True)
plt.yscale('log')
plt.xscale('log')
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.savefig(newpath+'/Gamma_match_TS25.pdf')
#plt.show()

#TS MATCH
fig = pl.figure(figsize=(8,6))
plt.xlabel(r'$TS$', fontsize=18)
plt.ylabel(r'$TS_{\rm{3FHL}}$', fontsize=18)
plt.errorbar(TS_our, TS_uff, fmt="o", color="black", ms=4.0) 
plt.plot(TS_uff, TS_uff, ls='--', lw=2.0, label='blue')
plt.axis([25.,1e4,25.,1e4], fontsize=18)
plt.grid(True)
plt.yscale('log')
plt.xscale('log')
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.savefig(newpath+'/TS_match_TS25.pdf')


vectso = [25,25,25,25,25]
vectst = [0.01,0.1,1.0,10.,100.]

#TS MATCH PAPER
fig = pl.figure(figsize=(8,6))
plt.xlabel(r'$TS$', fontsize=18)
plt.grid(True,ls=":", color='0.65')
plt.ylabel(r'$TS_{\rm{3FHL}}/TS$', fontsize=18)
plt.errorbar(TS_our, np.divide(TS_uff,TS_our), fmt=".", color="black", ms=10.0) 
#plt.plot(vectso,vectst,lw=2.0,ls='--',label=r'$TS=25$')
#plt.plot(TS_uff, TS_uff, ls='--', lw=2.0, label='blue')
plt.axis([24.,1e4,5e-1,1.7], fontsize=18)
plt.grid(True)
plt.yscale('linear')
plt.xscale('log')
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)

plt.savefig(newpath+'/TS_match_TS25def.pdf')
plt.savefig('../plot/TS_match_TS25def.pdf')
#plt.show()

sources=0.
match=0.
nomatch=0.
doublematch=0.
TSh=0.
TSl=0.

#Our catalog --> Ufficial
#print 'TS', 'TS_uff', 'GLON', 'GLON_uff', 'GLAT', 'GLAT_uff'
print 'sourcename uff','\t','GLAT uff', '\t', 'GLON uff', '\t','TS uff'
for i in range(len(GLAT)):
  if(np.abs(GLAT[i])>=glat_cut and TS[i]>=25.):
    local_match= 0
    sources= sources+1
    double_match=0.
    for j in range(len(GLAT_uff)):
  	if(np.abs(GLAT_uff[j])>=glat_cut and TSuff[j]>=25.):
    	  d= np.power( np.power( GLON[i]-GLON_uff[j] ,2.0) + np.power(GLAT[i]-GLAT_uff[j],2.0) ,0.5)
	  if(d<0.2):
	    local_match= 1.
	    match= match +1
	    double_match= double_match +1
	    if(TS[i]>TSuff[j]): TSh=TSh+1
	    elif(TS[i]<TSuff[j]): TSl=TSl+1
	    #print TS[i], TS_uff[j], GLON[i], GLON_uff[j], GLAT[i], GLAT_uff[j]
    if(double_match>=2.): 
	doublematch = doublematch + 1
	print 'DOUBLE MATCH', double_match , doublematch
	
	    
    if(local_match==0): 
      nomatch=nomatch+1
      print  source_name[i], '\t', GLAT[i], '\t', GLON[i],'\t', TS[i]

print 'number of matches=' ,match, 'among', sources, 'sources', 'no match', nomatch, 'double match',doublematch
print 'TS is higher than TS uff in cases', TSh
print 'TS is lesser than TS uff in cases', TSl
