"""
Class for reading catalog fits files
Version History:
----------------


created:      2017-14-02
last updated: 2017-16-02
"""

__author__= 'manconi@to.infn.it'
__version__=0.1



from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf


class catalog(object):
  
  
  def __init__(self, filename, sim, hdn, **kwargs):
    """
      Initialize class instance.
      
      Parameters:
      filename: path to filename of the catalog
      sim: True if the catalog has been obtained with the simulation
      
    """
    self.filename= filename
    self.sim= sim
    self.hdn= hdn
    self.sort=False
      
      
      
      
      
  def __call__(self):
    """
     Instance call
     
     read the fields?
     """
     
    hduls= pf.open(self.filename)
    self.head=(hduls[self.hdn].header)
    
    if(self.sim==False):
	self.source_name=(hduls[self.hdn].data.field('Source_Name')) 		
	self.GLON=(hduls[self.hdn].data.field('GLON'))  			
	self.GLAT=(hduls[self.hdn].data.field('GLAT')) 			
	self.TS=(hduls[self.hdn].data.field('ts'))			
	self.param_index=(hduls[self.hdn].data.field('param_values')[:,1]) 	
	self.param_index_err=(hduls[self.hdn].data.field('param_errors')[:,1])
	self.flux=(hduls[self.hdn].data.field('flux'))    	
	self.flux_err=(hduls[self.hdn].data.field('flux_err')) 
      
    
    elif(self.sim==True):
	self.source_name=(hduls[self.hdn].data.field('Source_Name')) 		
	self.GLON=(hduls[self.hdn].data.field('GLON'))  			
	self.GLAT=(hduls[self.hdn].data.field('GLAT')) 			
	self.flux=(hduls[self.hdn].data.field('flux'))
        self.param_index=(hduls[self.hdn].data.field('Index'))
    hduls.close()
     
     
   
   
   
  def read_field(self, fieldname):
      
      hduls= pf.open(self.filename)
      field=(hduls[self.hdn].data.field(fieldname))
      hduls.close()
      return field
    
  #def sort_bcut(self, bcut, field):
    #for i in range(len(self.GLAT)):
      #if(np.abs(self.GLAT[i]>bcut)):
	#sorted_field.append(field[i])
    #return sorted_field
	
	
	
	
  #def sort_TScut(self, TScut, field):
    #for i in range(len(self.TS)):
      #if(np.abs(self.TS[i]>TScut)):
	#sorted_field.append(field[i])
    #return sorted_field	
	
  #def sort_cat(self, TScut, bcut):
    #self.sort=True
    #self.source_name_sorted=[]
    #for i in range(len(self.source_name)):
       #if(np.abs(self.GLAT[i])>bcut and self.TS[i]>TScut):
	 #self.source_name_sorted.append(self.source_name[i])		
	 #self.GLON_sorted.append(self.GLON[i])			
	 #self.GLAT_sorted.append(self.GLAT[i])		
	 #self.TS_sorted.append(self.TS[i])			
	 #self.param_index_sorted.append(self.param_index[i])	
	 #self.param_index_err_sorted.append(self.param_index_err[i])
	 #self.flux_sorted.append(self.flux[i])   	
	 #self.flux_err_sorted.append(self.flux_err[i]) 
    #return 0.
 
  def sort_field(self, TScut, bcut, field):
    sorted_field=[]
    for i in range(len(field)):
       if(self.sort==False):
	 if(np.abs(self.GLAT[i])>bcut and self.TS[i]>TScut):
	   sorted_field.append(field[i])
    return sorted_field	   
  
  
  def sort_field_pro(self, TSmin,TSmax, bcut, field):
    sorted_field=[]
    for i in range(len(field)):
       if(self.sort==False):
	 if(np.abs(self.GLAT[i])>bcut and self.TS[i]>=TSmin and self.TS[i]<=TSmax):
	   sorted_field.append(field[i])
    return sorted_field	 
	 
       
	 
      
    
      
      
     
     
     