#---------------------------------------
#script to analyze detected PSRs regardless TScurv
#---------------------------------------
import astropy.io.fits as pyfits
from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
import random
from math import *
import matplotlib.pyplot as pl
import numpy as np
from astropy import units as unit
from astropy.coordinates import SkyCoord
import os.path


data_3fhl = pyfits.open("/Users/mattiadimauro/Dropbox/FERMI-LAT/3FHLanalysis/3FHL_paper/apj/data/gll_psc7year10GeV_v17r1_assoc_v7r6p1_classes_p3.fits")
data3fhl =data_3fhl[1].data

class3fhl  = data3fhl.field('class_sara')
glat3fhl  = data3fhl.field('GLAT')
glong3fhl  = data3fhl.field('GLON')
ts3fhl  = data3fhl.field('Test_Statistic')
name3fhl  = data3fhl.field('Source_Name')
flux3fhl  = data3fhl.field('Flux10000')
fluxerr3fhl  = data3fhl.field('Unc_Flux10000')

cont = 0
contextr = 0
contpsr = 0
contsnr = 0
contun = 0
contblazar = 0
for t in range(len(glat3fhl)):
    if abs(glat3fhl[t])>20.:
        if class3fhl[t]=='BLL' or class3fhl[t]=='FSRQ' or class3fhl[t]=='GAL' or class3fhl[t]=='RDG' or class3fhl[t]=='agn' or class3fhl[t]=='bcu' or class3fhl[t]=='bll' or class3fhl[t]=='fsrq' or class3fhl[t]=='rdg' or class3fhl[t]=='sbg':
            contextr = contextr + 1
        if class3fhl[t]=='BLL' or class3fhl[t]=='FSRQ' or class3fhl[t]=='agn' or class3fhl[t]=='bcu' or class3fhl[t]=='bll' or class3fhl[t]=='fsrq':
            contblazar = contblazar + 1
        if class3fhl[t]=='spp' or class3fhl[t]=='snr' or class3fhl[t]=='SNR':
            contsnr = contsnr + 1
            print contsnr
        if class3fhl[t]=='PSR' or class3fhl[t]=='PWN' or class3fhl[t]=='psr' or class3fhl[t]=='pwn' :
            contpsr = contpsr + 1
        if class3fhl[t]=='None' or class3fhl[t]=='unknown':
            contun = contun + 1
        cont = cont + 1
print contpsr,contsnr,contextr,contun,cont,contblazar
#8 0 888 88 986 876








