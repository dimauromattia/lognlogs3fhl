from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf

pigreco= np.pi

def dNdS_BPL(SS,norm,indexup,indexdown,breakF):
    if SS>breakF:
        return norm*np.power(SS,-indexup)
    if SS<=breakF:
        return norm*np.power(SS,-indexdown)*np.power(breakF,-indexup+indexdown)
        

def functheta(theta):
    return np.cos(theta)
    
solidangle20deg = 4.*(pigreco*180./180.)*romberg(functheta, pigreco*20./180.,pigreco*90./180.)*np.power(180./np.pi,2.)


def ratio_err(num, den, err_num, err_den):
  rel_num= np.divide(err_num,num)
  rel_den= np.divide(err_den,den)
  return np.sqrt(np.power(rel_num, 2) + np.power(-rel_den, 2))*(num/den)


data_3fhl = pf.open("../data/gll_psch_v13.fit")
data3fhl =data_3fhl[1].data

class3fhl  = data3fhl.field('CLASS')
flux3fhl  = data3fhl.field('Flux')
fluxerr3fhl  = data3fhl.field('Unc_Flux')

index = np.where((class3fhl=='rdg    ') | (class3fhl=='RDG    '))
flux_rdg = flux3fhl[index]

binwidth= 1e-11
Snbins= 6
Smax=6.e-10
Smin= 1.8e-11
Sbins= 10**np.linspace(np.log10(Smin), np.log10(Smax), Snbins) 
Sbins= np.concatenate((Sbins, [5.e-9]), axis=0)
Snbins= Snbins +1
#Compute centers of the bins
Sval = np.zeros(len(Sbins)-1) 
for j in range(len(Sbins)-2):
  #Sval[j]= 10.**((np.log10(Sbins[j+1])+ np.log10(Sbins[j]))/2.)
  Sval[j]= (Sbins[j+1]+Sbins[j])/2.
Sval[len(Sbins)-2]= (Sbins[len(Sbins)-2]+Sbins[len(Sbins)-1])/2.
#Compute DeltaS
deltaS=np.zeros(Snbins-1)
for i in range(len(deltaS)):
  deltaS[i]= Sbins[i+1]-Sbins[i]
print 'S val in the analysis', Sval

Ncounts_magn= np.histogram(flux_rdg,Sbins)

dNdS_magn= np.float_(Ncounts_magn[0])/deltaS
dNdS_magn_err= np.sqrt(np.float_(Ncounts_magn[0]))/deltaS
dNdS_magn[0]=dNdS_magn[0]/0.80
Sval_magn = Sval
deltaS_magn = deltaS

#OPEN EFF SIM BEST OFF

#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/dNdS_catcorr_102000_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS16.0.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS16= [x[0] for x in data]
    Serr_TS16= [x[1] for x in data]
    dNdS_catcorr_TS16= [x[2] for x in data]
    dNdS_catcorr_err_TS16=[x[3] for x in data] 

Sval_TS16 = np.array(Sval_TS16)
Serr_TS16 = np.array(Serr_TS16)
dNdS_catcorr_TS16= np.array(dNdS_catcorr_TS16)
dNdS_catcorr_err_TS16=np.array(dNdS_catcorr_err_TS16)

#with open("dNdS_catcorr_['sim30']_TS20.0.dat") as source_file:
with open("../data/dNdS_catcorr_102000_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS20.0.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS20= [x[0] for x in data]
    Serr_TS20= [x[1] for x in data]
    dNdS_catcorr_TS20= [x[2] for x in data]
    dNdS_catcorr_err_TS20=[x[3] for x in data] 
    
Sval_TS20 = np.array(Sval_TS20)
Serr_TS20 = np.array(Serr_TS20)
dNdS_catcorr_TS20= np.array(dNdS_catcorr_TS20)
dNdS_catcorr_err_TS20=np.array(dNdS_catcorr_err_TS20)

#with open("dNdS_catcorr_['sim30']_TS25.0.dat") as source_file:
with open("../data/dNdS_catcorr_102000_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS25= [x[0] for x in data]
    Serr_TS25= [x[1] for x in data]
    dNdS_catcorr_TS25= [x[2] for x in data]
    dNdS_catcorr_err_TS25=[x[3] for x in data] 
    
Sval_TS25 = np.array(Sval_TS25)
Serr_TS25 = np.array(Serr_TS25)
dNdS_catcorr_TS25= np.array(dNdS_catcorr_TS25)
dNdS_catcorr_err_TS25=np.array(dNdS_catcorr_err_TS25)

'''
R_TS16 = 1.0-np.array([ 0.79888268,  0.52606429,  0.17405643,  0.01953943,  0.01809955,
        0.01583113,  0.01470588,  0.        ,  0.        ,  0.        ])
errR_TS16 = np.array([ 0.06960173,  0.01867468,  0.00865342,  0.00372851,  0.00456565,
        0.00651399,  0.00855265,  0.        ,  0.        ,  0.        ])
        
R_TS20 = 1.0-np.array([ 0.75757576,  0.27591707,  0.09445209,  0.01953943,  0.01809955,
        0.01583113,  0.01470588,  0.        ,  0.        ,  0.        ])
errR_TS20 = np.array([ 0.13086908,  0.02369555,  0.00688456,  0.00372851,  0.00456565,
        0.00651399,  0.00855265,  0.        ,  0.        ,  0.        ])

R_TS25 = 1.0-np.array([ 0.09756098,  0.03789894,  0.01958042,  0.01809955,
        0.01583113,  0.01470588,  0.        ,  0.        ,  0.        ])
errR_TS25 = np.array([ 0.02950529,  0.00511408,  0.0037364 ,  0.00456565,
        0.00651399,  0.00855265,  0.        ,  0.        ,  0.        ])


#errR_TS16 = np.array([0.02124887, 0.02405785, 0.01650152, 0.00279842, 0.00639201, 0.00627844, 0.0, 0.0, 0.0, 0.0])
#errR_TS20 =np.array([0.13076697, 0.0199678, 0.01762311, 0.00279842, 0.00639201, 0.00627844, 0.0, 0.0, 0.0, 0.0])
#errR_TS25 = np.array([ 0.04567924, 0.00943168, 0.00372137, 0.00639201, 0.00627844, 0.0, 0.0, 0.0, 0.0])
#R_TS16 = 1.0-np.array([0.77801693, 0.55769454, 0.22751805, 0.02925195, 0.01008931, 0.00511234, 0.0, 0.0, 0.0, 0.0])
#R_TS20 = 1.0-np.array([0.695, 0.3136861, 0.15262912, 0.02925195, 0.01008931, 0.00511234, 0.0, 0.0, 0.0, 0.0])
#R_TS25 = 1.0-np.array([0.11475191, 0.08738472, 0.0300996, 0.01008931, 0.00511234, 0.0, 0.0, 0.0, 0.0])

dNdS_catcorr_err_TS16 = dNdS_catcorr_TS16*np.sqrt( np.power(dNdS_catcorr_err_TS16/dNdS_catcorr_TS16,2.) + np.power(errR_TS16/R_TS16,2.))
dNdS_catcorr_err_TS20 = dNdS_catcorr_TS20*np.sqrt( np.power(dNdS_catcorr_err_TS20/dNdS_catcorr_TS20,2.) + np.power(errR_TS20/R_TS20,2.))
dNdS_catcorr_err_TS25 = dNdS_catcorr_TS25*np.sqrt( np.power(dNdS_catcorr_err_TS25/dNdS_catcorr_TS25,2.) + np.power(errR_TS25/R_TS25,2.))
'''

#Svalfit = np.power(10.,np.arange(-15.,-6,0.01))

table_TS16 = np.loadtxt('../data/dNdS_bandfit_BPL_Nspurious_TS16_paper.dat')
Svalfit = table_TS16[:,0]
dNdS_down_TS16 = table_TS16[:,2]
dNdS_up_TS16 = table_TS16[:,3]
dNdS_fit_TS16 = table_TS16[:,1]

table_TS20 = np.loadtxt('../data/dNdS_bandfit_BPL_Nspurious_TS20_paper.dat')
#Svalfit = table_TS16[:,0]
dNdS_down_TS20 = table_TS20[:,2]
dNdS_up_TS20 = table_TS20[:,3]
dNdS_fit_TS20 = table_TS20[:,1]

table_TS25 = np.loadtxt('../data/dNdS_bandfit_BPL_Nspurious_TS25_paper.dat')
#Svalfit = table_TS16[:,0]
dNdS_down_TS25 = table_TS25[:,2]
dNdS_up_TS25 = table_TS25[:,3]
dNdS_fit_TS25 = table_TS25[:,1]


#dNdS_catcorr_TS16[1]=valor*1.10
#dNdS_catcorr_err_TS16[1]=valorerr*1.10
#print dNdS_fit_TS20*Svalfit*Svalfit

fig = plt.figure(figsize=(8,6))
plt.fill_between(Svalfit,dNdS_down_TS25,dNdS_up_TS25, color="#FFA500", alpha=0.5)
plt.plot(Svalfit,np.multiply(np.power(Svalfit, 2),dNdS_down_TS25)*1e-10, color="#FFA500", alpha=0.5, lw=10, label=r'$TS>25$, $1\sigma$ band')
plt.fill_between(Svalfit,dNdS_down_TS20,dNdS_up_TS20, color="#00FFFF", alpha=0.5)
plt.plot(Svalfit,np.multiply(np.power(Svalfit, 2),dNdS_down_TS20)*1e-10, color="#00FFFF", alpha=0.5, lw=10, label=r'$TS>20$, $1\sigma$ band')
plt.fill_between(Svalfit,dNdS_down_TS16,dNdS_up_TS16, color="#A9A9A9", alpha=0.5)
plt.plot(Svalfit,np.multiply(np.power(Svalfit, 2),dNdS_down_TS16)*1e-10, color="#A9A9A9", alpha=0.5, lw=10, label=r'$TS>16$, $1\sigma$ band')
plt.plot(Svalfit, dNdS_fit_TS20,color='blue',lw=1.5,label=r'$TS>20$, Best Fit')
plt.errorbar(np.multiply(Sval_TS25*1.05, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.95, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
#plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_Off_corrected_eff.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')



table_pred = np.loadtxt('../data/blazar_prediction_tuned.txt')
dNdS_pred_low = []
dNdS_pred_up = []
S_pred = []
for t in range(len(table_pred)/2):
    dNdS_pred_low.append(table_pred[t,1])
    dNdS_pred_up.append(table_pred[len(table_pred)-t-1,1])
    S_pred.append(table_pred[t,0])
    


table_pred_ple = np.loadtxt('../data/blazar_prediction_ple.txt')
dNdS_pred_low_ple = []
dNdS_pred_up_ple = []
S_pred_ple = []
for t in range(len(table_pred_ple)/2):
    dNdS_pred_low_ple.append(table_pred_ple[t,1])
    dNdS_pred_up_ple.append(table_pred_ple[len(table_pred_ple)-t-1,1])
    S_pred_ple.append(table_pred_ple[t,0])
    
table_pred_ldde = np.loadtxt('../data/blazar_prediction_ldde.txt')
dNdS_pred_low_ldde = []
dNdS_pred_up_ldde = []
S_pred_ldde = []
for t in range(len(table_pred_ldde)/2):
    dNdS_pred_low_ldde.append(table_pred_ldde[t,1])
    dNdS_pred_up_ldde.append(table_pred_ldde[len(table_pred_ldde)-t-1,1])
    S_pred_ldde.append(table_pred_ldde[t,0])

    
table_1fhl = np.loadtxt('../data/dNdS_1FHL_corr.txt')
dNdS_1fhl = []
dNdSerr_1fhl = []
S_1fhl = []
Serr_1fhl = []
for t in range(len(table_1fhl)):
    dNdS_1fhl.append(table_1fhl[t,2])
    dNdSerr_1fhl.append(table_1fhl[t,3])
    S_1fhl.append(table_1fhl[t,0])
    Serr_1fhl.append(table_1fhl[t,1])

S_1fhl=np.array(S_1fhl)
Serr_1fhl=np.array(Serr_1fhl)
dNdS_1fhl=np.array(dNdS_1fhl)
dNdSerr_1fhl=np.array(dNdSerr_1fhl)


fig = plt.figure(figsize=(8,6))
plt.fill_between(S_pred,dNdS_pred_low,dNdS_pred_up, color="#DEB887", alpha=0.5)
plt.errorbar(np.multiply(S_1fhl*1.0, 1), np.multiply(dNdS_1fhl, np.power(S_1fhl, 2)), xerr= Serr_1fhl, yerr= np.multiply(dNdSerr_1fhl, np.power(S_1fhl, 2)), color='blue', fmt='d', label='1FHL')

#plt.plot(S_pred,np.multiply(np.power(Svalband, 2),dNdS_down)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'$\omega$, $1\sigma$ band')
#plt.plot(Svalfit, dNdS_fit*Svalfit*Svalfit,color='black',lw=1.5,label=r'$\omega$, Best Fit')
plt.errorbar(np.multiply(Sval_TS16*1.0, 1), np.multiply(dNdS_catcorr_TS16, np.power(Sval_TS16, 2))/solidangle20deg, xerr= Serr_TS16, yerr= np.multiply(dNdS_catcorr_err_TS16, np.power(Sval_TS16, 2))/solidangle20deg, color='black', fmt='d', label='$TS>16$')
#plt.errorbar(np.multiply(Sval_TS20*1.00, 1), np.multiply(dNdS_catcorr_TS20, np.power(Sval_TS20, 2))/solidangle20deg, xerr= Serr_TS20, yerr= np.multiply(dNdS_catcorr_err_TS20, np.power(Sval_TS20, 2))/solidangle20deg, color='blue', fmt='>', label='$TS>20$')
#plt.errorbar(np.multiply(Sval_TS25*1.05, 1), np.multiply(dNdS_catcorr_TS25, np.power(Sval_TS25, 2))/solidangle20deg, xerr= Serr_TS25, yerr= np.multiply(dNdS_catcorr_err_TS25, np.power(Sval_TS25, 2))/solidangle20deg, color='red', fmt='*', label='$TS>25$')
#plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
#plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Fit LP')
#plt.errorbar(Sval_1FHLcorr, np.array(dNdS_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr),xerr= deltaS_1FHLcorr, yerr= np.array(dNdSerr_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr), color='orange', fmt='*', label='1FHL corr')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,1e-11], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_pred_eff.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')

'''
systematics [ 0.1342455   0.0579506   0.0462076   0.01916293  0.04740268  0.07257838
  0.04906544  0.02897328  0.07723341  0.01460262]
systematics [ 0.08892722  0.05628328  0.01916543  0.04794961  0.07257838  0.04906544

'''

dNdS_sys_25 = [0.09,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05]
dNdS_sys_16 = [0.14,0.06,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05]
dNdS_sys_25=np.array(dNdS_sys_25)
dNdS_sys_16=np.array(dNdS_sys_16)
dNdS_catcorr_err_TS16=np.array(dNdS_catcorr_err_TS16)
dNdS_catcorr_err_TS25=np.array(dNdS_catcorr_err_TS25)
for t in range(len(dNdS_catcorr_err_TS16)):
    dNdS_catcorr_err_TS16[t] = dNdS_catcorr_err_TS16[t]*(1.+dNdS_sys_16[t])

for t in range(len(dNdS_catcorr_err_TS25)):
    dNdS_catcorr_err_TS25[t] = dNdS_catcorr_err_TS25[t]*(1.+dNdS_sys_25[t])
    
#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/dNdS_10_1000_GeV_1pPDF.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_pdf= [x[0] for x in data]
    dNdS_pdf=[x[1] for x in data] 
    dNdSl_pdf= [x[2] for x in data]
    dNdSu_pdf=[x[3] for x in data] 

Sval_pdf = np.array(Sval_pdf)
dNdSu_pdf= np.array(dNdSu_pdf)
dNdSl_pdf=np.array(dNdSl_pdf)
dNdS_pdf=np.array(dNdS_pdf)

sy = np.loadtxt('../data/dNdS_10_1000_GeV_1pPDF_syst.dat')

dNdS_pred_low_ldde = np.array(dNdS_pred_low_ldde)
dNdS_pred_low_ple = np.array(dNdS_pred_low_ple)

fig = plt.figure(figsize=(8,6))
plt.fill_between(S_pred_ldde,dNdS_pred_low_ldde,dNdS_pred_up_ldde, color="#00FFFF", alpha=0.5)
plt.plot(S_pred_ldde,dNdS_pred_low_ldde*1e-10, color="#00FFFF", alpha=0.5, lw=10, label='Blazars LDDE')
plt.fill_between(sy[:,0],sy[:,0]**2*sy[:,1],sy[:,0]**2*sy[:,2],facecolor='#EEDBC3',alpha=0.5,lw=0)
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band')
plt.errorbar(np.multiply(S_1fhl*1.0, 1), np.multiply(dNdS_1fhl, np.power(S_1fhl, 2)), xerr= Serr_1fhl, yerr= np.multiply(dNdSerr_1fhl, np.power(S_1fhl, 2)), color='blue', fmt='d', label='1FHL')
plt.errorbar(np.multiply(Sval_TS25*1.02, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
#plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.98, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
#plt.errorbar(np.multiply(Sval_magn*1.05, 1), np.multiply(dNdS_magn, np.power(Sval_magn, 2))/solidangle20deg, xerr= deltaS_magn/2., yerr= np.multiply(dNdS_magn_err, np.power(Sval_magn, 2))/solidangle20deg, color='green', fmt='o', label='MAGN')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,3e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_pred_paper_ldde.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')


fig = plt.figure(figsize=(8,6))
plt.fill_between(S_pred_ldde,dNdS_pred_low_ldde,dNdS_pred_up_ldde, color="#00FFFF", alpha=0.5)
plt.plot(S_pred_ldde,dNdS_pred_low_ldde*1e-10, color="#00FFFF", alpha=0.5, lw=10, label='Blazars LDDE')
plt.fill_between(sy[:,0],sy[:,0]**2*sy[:,1],sy[:,0]**2*sy[:,2],facecolor='#EEDBC3',alpha=0.5,lw=0)
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band')
#plt.errorbar(np.multiply(S_1fhl*1.0, 1), np.multiply(dNdS_1fhl, np.power(S_1fhl, 2)), xerr= Serr_1fhl, yerr= np.multiply(dNdSerr_1fhl, np.power(S_1fhl, 2)), color='blue', fmt='d', label='1FHL')
plt.errorbar(np.multiply(Sval_TS25*1.02, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
#plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.98, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
#plt.errorbar(np.multiply(Sval_magn*1.05, 1), np.multiply(dNdS_magn, np.power(Sval_magn, 2))/solidangle20deg, xerr= deltaS_magn/2., yerr= np.multiply(dNdS_magn_err, np.power(Sval_magn, 2))/solidangle20deg, color='green', fmt='o', label='MAGN')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,3e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_pred_proporal_ldde.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')



fig = plt.figure(figsize=(8,6))
plt.fill_between(S_pred_ldde,dNdS_pred_low_ldde,dNdS_pred_up_ldde, color="#00FFFF", alpha=0.5)
plt.plot(S_pred_ldde,dNdS_pred_low_ldde*1e-10, color="#00FFFF", alpha=0.5, lw=10, label='Blazars LDDE')
#plt.fill_between(sy[:,0],sy[:,0]**2*sy[:,1],sy[:,0]**2*sy[:,2],facecolor='#EEDBC3',alpha=0.5,lw=0)
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band')
#plt.errorbar(np.multiply(S_1fhl*1.0, 1), np.multiply(dNdS_1fhl, np.power(S_1fhl, 2)), xerr= Serr_1fhl, yerr= np.multiply(dNdSerr_1fhl, np.power(S_1fhl, 2)), color='blue', fmt='d', label='1FHL')
plt.errorbar(np.multiply(Sval_TS25*1.02, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
#plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.98, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
#plt.errorbar(np.multiply(Sval_TS25*1.05, 1), np.multiply(dNdS_catcorr_TS25, np.power(Sval_TS25, 2))/solidangle20deg, xerr= Serr_TS25, yerr= np.multiply(dNdS_catcorr_err_TS25, np.power(Sval_TS25, 2))/solidangle20deg, color='red', fmt='*', label='$TS>25$')
plt.errorbar(np.multiply(Sval_magn*1.05, 1), np.multiply(dNdS_magn, np.power(Sval_magn, 2))/solidangle20deg, xerr= deltaS_magn/2., yerr= np.multiply(dNdS_magn_err, np.power(Sval_magn, 2))/solidangle20deg, color='green', fmt='o', label='MAGN')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,2e-15,2e-11], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=2,prop={'size':16},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_pred_paper_ldde_magn.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')


dNdS_magn= np.float_(Ncounts_magn[0])/deltaS
dNdS_magn_err= np.sqrt(np.float_(Ncounts_magn[0]))/deltaS
Sval_magn = Sval

fig = plt.figure(figsize=(8,6))
plt.fill_between(S_pred_ple,dNdS_pred_low_ple,dNdS_pred_up_ple, color="#00FFFF", alpha=0.5)
plt.plot(S_pred_ple,dNdS_pred_low_ple*1e-10, color="#00FFFF", alpha=0.5, lw=10, label='Blazars PLE')
plt.fill_between(sy[:,0],sy[:,0]**2*sy[:,1],sy[:,0]**2*sy[:,2],facecolor='#EEDBC3',alpha=0.5,lw=0)
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band')
plt.errorbar(np.multiply(S_1fhl*1.0, 1), np.multiply(dNdS_1fhl, np.power(S_1fhl, 2)), xerr= Serr_1fhl, yerr= np.multiply(dNdSerr_1fhl, np.power(S_1fhl, 2)), color='blue', fmt='d', label='1FHL')
plt.errorbar(np.multiply(Sval_TS25*1.05, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
#plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.95, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
#plt.errorbar(np.multiply(Sval_TS20*1.00, 1), np.multiply(dNdS_catcorr_TS20, np.power(Sval_TS20, 2))/solidangle20deg, xerr= Serr_TS20, yerr= np.multiply(dNdS_catcorr_err_TS20, np.power(Sval_TS20, 2))/solidangle20deg, color='blue', fmt='>', label='$TS>20$')
#plt.errorbar(np.multiply(Sval_TS25*1.05, 1), np.multiply(dNdS_catcorr_TS25, np.power(Sval_TS25, 2))/solidangle20deg, xerr= Serr_TS25, yerr= np.multiply(dNdS_catcorr_err_TS25, np.power(Sval_TS25, 2))/solidangle20deg, color='red', fmt='*', label='$TS>25$')
#plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
#plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Fit LP')
#plt.errorbar(Sval_1FHLcorr, np.array(dNdS_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr),xerr= deltaS_1FHLcorr, yerr= np.array(dNdSerr_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr), color='orange', fmt='*', label='1FHL corr')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
#plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,3e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_pred_paper_ple.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')


#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/dNdS_10_1000_GeV_1pPDF.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_pdf= [x[0] for x in data]
    #dNdS_pdf=[x[1] for x in data] 
    dNdSl_pdf= [x[2] for x in data]
    dNdSu_pdf=[x[3] for x in data] 
    
#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/dNdS_10_1000_GeV_1pPDF_best.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sfit_pdf= [x[0] for x in data]
    dNdS_pdf=[x[1] for x in data] 

Sfit_pdf = np.array(Sfit_pdf)
Sval_pdf = np.array(Sval_pdf)
dNdSu_pdf= np.array(dNdSu_pdf)
dNdSl_pdf=np.array(dNdSl_pdf)
dNdS_pdf=np.array(dNdS_pdf)

sy = np.loadtxt('../data/dNdS_10_1000_GeV_1pPDF_syst.dat')

#dNdS_pdf[0:48]=0
fig = plt.figure(figsize=(8,6))
plt.fill_between(sy[:,0],sy[:,0]**2*sy[:,1],sy[:,0]**2*sy[:,2],facecolor='#EEDBC3',alpha=0.5,lw=0)
plt.plot(sy[:,0],sy[:,0]**2*sy[:,1]*1E-10,color='#EEDBC3',alpha=0.5,lw=10, label=r'1pPDF, $1\sigma$ band SYS')
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band STAT')
plt.plot(Sfit_pdf, dNdS_pdf*Sfit_pdf*Sfit_pdf,color='black',lw=1.5,label='1pPDF, Best Fit')
plt.errorbar(np.multiply(Sval_TS25*1.05, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.95, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
#plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
#plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Fit LP')
#plt.errorbar(Sval_1FHLcorr, np.array(dNdS_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr),xerr= deltaS_1FHLcorr, yerr= np.array(dNdSerr_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr), color='orange', fmt='*', label='1FHL corr')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
#plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_Off_corrected_1pPDF.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')


#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/3fhl_data/fig12_left/dNdS_10_1000_GeV.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_pdf= [x[0] for x in data]
    #dNdSb_pdf=[x[1] for x in data] 
    dNdSl_pdf= [x[2] for x in data]
    dNdSu_pdf=[x[3] for x in data] 
    
#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/3fhl_data/fig12_left/dNdS_10_1000_GeV_best.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sfit_pdf= [x[0] for x in data]
    dNdS_pdf=[x[1] for x in data] 

Sfit_pdf = np.array(Sfit_pdf)
Sval_pdf = np.array(Sval_pdf)
dNdSu_pdf= np.array(dNdSu_pdf)
dNdSl_pdf=np.array(dNdSl_pdf)
dNdS_pdf=np.array(dNdS_pdf)

sy = np.loadtxt('../data/3fhl_data/fig12_left/dNdS_10_1000_GeV_1pPDF_syst.dat')

#dNdS_pdf[0:48]=0
fig = plt.figure(figsize=(8,6))
plt.fill_between(sy[:,0],sy[:,0]**2*sy[:,1],sy[:,0]**2*sy[:,2],facecolor='#EEDBC3',alpha=0.5,lw=0)
plt.plot(sy[:,0],sy[:,0]**2*sy[:,1]*1E-10,color='#EEDBC3',alpha=0.5,lw=10, label=r'1pPDF, $1\sigma$ band SYS')
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band STAT')
plt.plot(Sfit_pdf, dNdS_pdf*Sfit_pdf*Sfit_pdf,color='black',lw=1.5,label='1pPDF, Best Fit')
plt.errorbar(np.multiply(Sval_TS25*1.05, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.95, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
#plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
#plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Fit LP')
#plt.errorbar(Sval_1FHLcorr, np.array(dNdS_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr),xerr= deltaS_1FHLcorr, yerr= np.array(dNdSerr_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr), color='orange', fmt='*', label='1FHL corr')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
#plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_Off_corrected_1pPDF_HP7_source.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')



#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/3fhl_data/fig12_right/dNdS_10_1000_GeV.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_pdf= [x[0] for x in data]
    #dNdSb_pdf=[x[1] for x in data] 
    dNdSl_pdf= [x[2] for x in data]
    dNdSu_pdf=[x[3] for x in data] 
    
#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/3fhl_data/fig12_right/dNdS_10_1000_GeV_best.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sfit_pdf= [x[0] for x in data]
    dNdS_pdf=[x[1] for x in data] 

Sfit_pdf = np.array(Sfit_pdf)
Sval_pdf = np.array(Sval_pdf)
dNdSu_pdf= np.array(dNdSu_pdf)
dNdSl_pdf=np.array(dNdSl_pdf)
dNdS_pdf=np.array(dNdS_pdf)

sy = np.loadtxt('../data/3fhl_data/fig12_right/dNdS_10_1000_GeV_1pPDF_syst.dat')

#dNdS_pdf[0:48]=0
fig = plt.figure(figsize=(8,6))
plt.fill_between(sy[:,0],sy[:,0]**2*sy[:,1],sy[:,0]**2*sy[:,2],facecolor='#EEDBC3',alpha=0.5,lw=0)
plt.plot(sy[:,0],sy[:,0]**2*sy[:,1]*1E-10,color='#EEDBC3',alpha=0.5,lw=10, label=r'1pPDF, $1\sigma$ band SYS')
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band STAT')
plt.plot(Sfit_pdf, dNdS_pdf*Sfit_pdf*Sfit_pdf,color='black',lw=1.5,label='1pPDF, Best Fit')
plt.errorbar(np.multiply(Sval_TS25*1.05, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.95, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
#plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
#plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Fit LP')
#plt.errorbar(Sval_1FHLcorr, np.array(dNdS_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr),xerr= deltaS_1FHLcorr, yerr= np.array(dNdSerr_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr), color='orange', fmt='*', label='1FHL corr')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
#plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_Off_corrected_1pPDF_HP7_clean.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')



#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/3fhl_data/fig13/dNdS_10_1000_GeV.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_pdf= [x[0] for x in data]
    #dNdSb_pdf=[x[1] for x in data] 
    dNdSl_pdf= [x[2] for x in data]
    dNdSu_pdf=[x[3] for x in data] 
    
#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/3fhl_data/fig13/dNdS_10_1000_GeV_best.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sfit_pdf= [x[0] for x in data]
    dNdS_pdf=[x[1] for x in data] 

Sfit_pdf = np.array(Sfit_pdf)
Sval_pdf = np.array(Sval_pdf)
dNdSu_pdf= np.array(dNdSu_pdf)
dNdSl_pdf=np.array(dNdSl_pdf)
dNdS_pdf=np.array(dNdS_pdf)

sy = np.loadtxt('../data/3fhl_data/fig13/dNdS_10_1000_GeV_1pPDF_syst.dat')

#dNdS_pdf[0:48]=0
fig = plt.figure(figsize=(8,6))
plt.fill_between(sy[:,0],sy[:,0]**2*sy[:,1],sy[:,0]**2*sy[:,2],facecolor='#EEDBC3',alpha=0.5,lw=0)
plt.plot(sy[:,0],sy[:,0]**2*sy[:,1]*1E-10,color='#EEDBC3',alpha=0.5,lw=10, label=r'1pPDF, $1\sigma$ band SYS')
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band STAT')
plt.plot(Sfit_pdf, dNdS_pdf*Sfit_pdf*Sfit_pdf,color='black',lw=1.5,label='1pPDF, Best Fit')
plt.errorbar(np.multiply(Sval_TS25*1.05, 1), dNdS_catcorr_TS25, xerr= Serr_TS25, yerr= dNdS_catcorr_err_TS25, color='red', fmt='*', label='$TS>25$')
plt.errorbar(np.multiply(Sval_TS20*1.00, 1), dNdS_catcorr_TS20, xerr= Serr_TS20, yerr= dNdS_catcorr_err_TS20, color='blue', fmt='>', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS16*0.95, 1), dNdS_catcorr_TS16, xerr= Serr_TS16, yerr= dNdS_catcorr_err_TS16, color='black', fmt='d', label='$TS>16$')
#plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
#plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Fit LP')
#plt.errorbar(Sval_1FHLcorr, np.array(dNdS_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr),xerr= deltaS_1FHLcorr, yerr= np.array(dNdSerr_1FHL)*np.array(Sval_1FHLcorr)*np.array(Sval_1FHLcorr), color='orange', fmt='*', label='1FHL corr')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
#plt.ylabel(r'$S^2 dN/dS_{\rm{corr}}$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_Off_corrected_1pPDF_HP8_ucv.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')





#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/dNdS_10_1000_GeV_1pPDF_sim.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_pdf= [x[0] for x in data]
    dNdS_pdf=[x[1] for x in data] 
    dNdSl_pdf= [x[2] for x in data]
    dNdSu_pdf=[x[3] for x in data] 

Sval_pdf = np.array(Sval_pdf)
dNdSu_pdf= np.array(dNdSu_pdf)
dNdSl_pdf=np.array(dNdSl_pdf)
dNdS_pdf=np.array(dNdS_pdf)

#with open("dNdS_catcorr_['sim30']_TS16.0.dat") as source_file:
with open("../data/dNdS_simulation_1pdf.txt") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_sim= [x[0] for x in data]
    Serr_sim= [x[1] for x in data]
    dNdS_sim= [x[2] for x in data]
    dNdS_err_sim=[x[3] for x in data] 

Sval_sim = np.array(Sval_sim)
Serr_sim = np.array(Serr_sim)
dNdS_sim= np.array(dNdS_sim)
dNdS_err_sim=np.array(dNdS_err_sim)


indexup = 2.1840 #index above the break
indexdown = 1.55 #index below the break
norm = np.power(10.,-13.94305)#1/((ph/cm^2/s))/deg^2
breakF = np.power(10.,-10.1738)

#Svalfit = np.power(10.,np.arange(-13.,-8.,0.005))
dNdS_fit = np.zeros(len(Svalfit))
for h in range(len(Svalfit)):
  dNdS_fit[h] = dNdS_BPL(Svalfit[h], norm, indexup, indexdown, breakF)


fig = plt.figure(figsize=(8,6))
plt.fill_between(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.),dNdSu_pdf*np.power(Sval_pdf,2.), color="#DEB887", alpha=0.5)
plt.plot(Sval_pdf,dNdSl_pdf*np.power(Sval_pdf,2.)*1e-10, color="#DEB887", alpha=0.5, lw=10, label=r'1pPDF, $1\sigma$ band')
plt.plot(Sval_pdf, dNdS_pdf*Sval_pdf*Sval_pdf,color='black',lw=1.5,label='1pPDF, Best Fit')
plt.errorbar(np.multiply(Sval_sim*0.95, 1), dNdS_sim, xerr= Serr_sim, yerr= dNdS_err_sim, color='blue', fmt='d', label='SIM data')
plt.plot(Svalfit, dNdS_fit*Svalfit*Svalfit,color='blue',lw=2.0, ls='--', label=r'SIM input')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [ph cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph cm$^{-2}$ s$^{-1}$ deg$^{-2}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,1e-14,2e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
#plt.savefig('dNdS_3FHL_corrected_fit_PL.pdf')
plt.savefig('../plot/dNdS_3FHL_simulation_1pPDF.pdf')
#plt.savefig('dNdS_3FHL_corrected_fit_LP.pdf')



Svalband=np.array([  1.00000000e-12,   1.58489319e-12,   2.51188643e-12,
         3.98107171e-12,   6.30957344e-12,   1.00000000e-11,
         1.58489319e-11,   2.51188643e-11,   3.98107171e-11,
         6.30957344e-11,   1.00000000e-10,   1.58489319e-10,
         2.51188643e-10,   3.98107171e-10,   6.30957344e-10,
         1.00000000e-09,   1.58489319e-09,   2.51188643e-09,
         3.98107171e-09,   6.30957344e-09,   1.00000000e-08,
         1.58489319e-08,   2.51188643e-08,   3.98107171e-08,
         6.30957344e-08])
dNdS_down=np.array([  1.83500778e+10,   1.52114806e+10,   1.16828242e+10,
         8.31317588e+09,   5.48060965e+09,   3.34759991e+09,
         1.89444063e+09,   9.62894620e+08,   4.39029404e+08,
         1.93495964e+08,   8.24353847e+07,   3.39483666e+07,
         1.24991149e+07,   4.14335950e+06,   1.27253211e+06,
         3.62099276e+05,   9.54617383e+04,   2.33170602e+04,
         5.27668326e+03,   1.10634616e+03,   2.14913557e+02,
         3.86793678e+01,   6.44967301e+00,   9.96411661e-01,
         1.42620738e-01])
dNdS_up=np.array([  9.09075070e+10,   5.25627703e+10,   2.93781649e+10,
         1.58722850e+10,   8.28929699e+09,   4.18466482e+09,
         2.04209826e+09,   9.93746624e+08,   4.82660602e+08,
         2.17201965e+08,   9.05583343e+07,   3.54539425e+07,
         1.36346752e+07,   5.20500326e+06,   1.93623764e+06,
         6.96241275e+05,   2.42004994e+05,   8.13115701e+04,
         2.64085053e+04,   8.29084414e+03,   2.51603765e+03,
         7.38072418e+02,   2.09287973e+02,   5.73657957e+01,
         1.51993575e+01])
Sval=np.array([  8.39943859e-12,   1.55160084e-11,   2.86622152e-11,
         5.29467735e-11,   9.78068443e-11,   1.80675387e-10,
         3.33755738e-10,   6.16536067e-10,   1.05000000e-09,
         3.15000000e-09])
dNdS_simdet_corr=np.array([  1.83721257e+14,   5.20605775e+13,   2.30366817e+13,
         7.42987233e+12,   2.36316932e+12,   7.72285578e+11,
         2.52891567e+11,   3.54293070e+10,   2.00000000e+10,
         8.10810811e+08])
dNdS_simdet_corr_err=np.array([  4.71074966e+13,   4.06138481e+12,   1.75532739e+12,
         7.81292081e+11,   3.24658273e+11,   1.55176594e+11,
         6.51220924e+10,   1.59204608e+10,   1.51657509e+10,
         6.19266986e+08])
Serr=np.array([  2.49943859e-12,   4.61713122e-12,   8.52907562e-12,
         1.57554826e-11,   2.91045882e-11,   5.37639546e-11,
         9.93163962e-11,   1.83463933e-10,   2.50000000e-10,
         1.85000000e-09])
Svalfit = np.power(10.,np.arange(-13.,-8.,0.1))
dNdS_sim=np.array([  4.70086794e+11,   4.10740664e+11,   3.55101233e+11,
         3.03760633e+11,   2.57102102e+11,   2.15315154e+11,
         1.78417888e+11,   1.46284066e+11,   1.18672607e+11,
         9.52574101e+10,   7.56557406e+10,   5.94538297e+10,
         4.62287991e+10,   3.55664255e+10,   2.70746379e+10,
         2.03929438e+10,   1.51981953e+10,   1.12072471e+10,
         8.17712624e+09,   5.90333309e+09,   4.21685536e+09,
         2.98040272e+09,   2.08427983e+09,   1.44222139e+09,
         9.87421699e+08,   6.68910836e+08,   4.48361823e+08,
         2.97360892e+08,   1.95134431e+08,   1.26700635e+08,
         8.13988940e+07,   5.17431719e+07,   3.25448610e+07,
         2.02538050e+07,   1.24717001e+07,   7.59870390e+06,
         4.58087261e+06,   2.73244694e+06,   1.61268702e+06,
         9.41766574e+05,   5.44165851e+05,   3.11110111e+05,
         1.75991543e+05,   9.85063665e+04,   5.45546321e+04,
         2.98946720e+04,   1.62087970e+04,   8.69566108e+03,
         4.61582415e+03,   2.42432442e+03])
dNdS_corr=np.array([  1.36107958e+11,   1.28799126e+11,   1.20298304e+11,
         1.10897892e+11,   1.00903045e+11,   9.06154906e+10,
         8.03189119e+10,   7.02668332e+10,   6.06736527e+10,
         5.17091139e+10,   4.34961928e+10,   3.61120871e+10,
         2.95917827e+10,   2.39335358e+10,   1.91055609e+10,
         1.50532376e+10,   1.17062349e+10,   8.98507588e+09,
         6.80680732e+09,   5.08958487e+09,   3.75611136e+09,
         2.73597266e+09,   1.96699040e+09,   1.39575735e+09,
         9.77540594e+08,   6.75735716e+08,   4.61037383e+08,
         3.10464969e+08,   2.06350850e+08,   1.35368343e+08,
         8.76486357e+07,   5.60132024e+07,   3.53307486e+07,
         2.19954307e+07,   1.35154123e+07,   8.19678076e+06,
         4.90653087e+06,   2.89883133e+06,   1.69039637e+06,
         9.72907057e+05,   5.52676946e+05,   3.09876427e+05,
         1.71483732e+05,   9.36643894e+04,   5.04944076e+04,
         2.68676234e+04,   1.41101755e+04,   7.31396330e+03,
         3.74188410e+03,   1.88949210e+03])

fig = plt.figure(figsize=(8,6))
plt.fill_between(Svalband,np.multiply(np.power(Svalband, 2),dNdS_down), np.multiply(np.power(Svalband, 2),dNdS_up), color="#DEB887", alpha=0.5)
plt.plot(Svalband,np.multiply(np.power(Svalband, 2),dNdS_down)*1e-10, lw=10, color="#DEB887", alpha=0.5,label=r'$\omega$, $1\sigma$ band')
plt.errorbar(np.multiply(Sval, 1), np.multiply(dNdS_simdet_corr, np.power(Sval, 2))/solidangle20deg, xerr= Serr, yerr= np.multiply(dNdS_simdet_corr_err, np.power(Sval, 2))/solidangle20deg, color='black', fmt='*', label='$TS>16$')
plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Best Fit LP')
plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [photon/cm$^2$/s]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph/cm$^2$/s/deg$^2$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,5e-14,1.5e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=3,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
plt.savefig('../plot/dNdS_LP_sim.pdf')


Svalband=np.array([  1.00000000e-13,   1.58489319e-13,   2.51188643e-13,
         3.98107171e-13,   6.30957344e-13,   1.00000000e-12,
         1.58489319e-12,   2.51188643e-12,   3.98107171e-12,
         6.30957344e-12,   1.00000000e-11,   1.58489319e-11,
         2.51188643e-11,   3.98107171e-11,   6.30957344e-11,
         1.00000000e-10,   1.58489319e-10,   2.51188643e-10,
         3.98107171e-10,   6.30957344e-10,   1.00000000e-09,
         1.58489319e-09,   2.51188643e-09,   3.98107171e-09,
         6.30957344e-09,   1.00000000e-08,   1.58489319e-08,
         2.51188643e-08,   3.98107171e-08,   6.30957344e-08])
dNdS_down=np.array([  1.49622989e+12,   8.03523027e+11,   4.31517415e+11,
         2.31738572e+11,   1.24450982e+11,   6.68341343e+10,
         3.58920552e+10,   1.92751749e+10,   1.03513818e+10,
         5.55902116e+09,   2.98537112e+09,   1.60323922e+09,
         8.10004522e+08,   3.62648663e+08,   1.56492354e+08,
         6.30798305e+07,   2.51873161e+07,   9.98492653e+06,
         3.72892317e+06,   1.32280471e+06,   4.57726607e+05,
         1.53479287e+05,   4.97076793e+04,   1.59683711e+04,
         5.12976823e+03,   1.64791525e+03,   5.29385449e+02,
         1.70062723e+02,   5.46318938e+01,   1.75502531e+01])
dNdS_up=np.array([  1.75180041e+13,   7.64688614e+12,   3.33798687e+12,
         1.45708412e+12,   6.36040286e+11,   2.77641655e+11,
         1.21194978e+11,   5.29035267e+10,   2.30932270e+10,
         1.00805592e+10,   4.40032368e+09,   1.92081095e+09,
         8.92913477e+08,   4.69316129e+08,   2.09803584e+08,
         8.63454261e+07,   3.62102651e+07,   1.45290711e+07,
         4.66739951e+06,   1.67248725e+06,   6.73629245e+05,
         2.71318277e+05,   1.09279115e+05,   4.40144507e+04,
         1.77277412e+04,   7.14273382e+03,   2.87913389e+03,
         1.16053771e+03,   4.67796158e+02,   1.88561943e+02])
Sval=np.array([  8.39943859e-12,   1.55160084e-11,   2.86622152e-11,
         5.29467735e-11,   9.78068443e-11,   1.80675387e-10,
         3.33755738e-10,   6.16536067e-10,   1.05000000e-09,
         3.15000000e-09])
dNdS_simdet_corr=np.array([  1.43532232e+14,   4.94693270e+13,   1.85476606e+13,
         6.81071630e+12,   2.02757131e+12,   5.70438211e+11,
         1.74214191e+11,   3.98579704e+10,   1.00000000e+10,
         1.35135135e+09])
dNdS_simdet_corr_err=np.array([  3.42385118e+13,   3.22670636e+12,   1.17025705e+12,
         5.90012265e+11,   2.33350370e+11,   9.81410000e+10,
         4.04318622e+10,   1.40282426e+10,   6.85930026e+09,
         7.67420047e+08])
Serr=np.array([  2.49943859e-12,   4.61713122e-12,   8.52907562e-12,
         1.57554826e-11,   2.91045882e-11,   5.37639546e-11,
         9.93163962e-11,   1.83463933e-10,   2.50000000e-10,
         1.85000000e-09])
Svalfit = np.power(10.,np.arange(-13.,-8.,0.1))
dNdS_sim=np.array([  3.16513446e+12,   2.24074360e+12,   1.58632499e+12,
         1.12303209e+12,   7.95045831e+11,   5.62849345e+11,
         3.98466821e+11,   2.82092906e+11,   1.99706484e+11,
         1.41381363e+11,   1.00090340e+11,   7.08585343e+10,
         5.01640006e+10,   3.55133928e+10,   2.51415567e+10,
         1.77988591e+10,   1.26006273e+10,   8.92056095e+09,
         6.31527352e+09,   4.47087126e+09,   3.16513446e+09,
         2.24074360e+09,   1.58632499e+09,   1.12303209e+09,
         7.95045831e+08,   5.62849345e+08,   3.98466821e+08,
         2.82092906e+08,   1.92796009e+08,   1.16171084e+08,
         7.00000000e+07,   4.21791710e+07,   2.54154638e+07,
         1.53143314e+07,   9.22779717e+06,   5.56029764e+06,
         3.35041065e+06,   2.01882205e+06,   1.21646058e+06,
         7.32989984e+05,   4.41670141e+05,   2.66132577e+05,
         1.60360736e+05,   9.66268985e+04,   5.82234640e+04,
         3.50831064e+04,   2.11396620e+04,   1.27379060e+04,
         7.67534737e+03,   4.62485414e+03])
dNdS_corr=np.array([  6.57985434e+12,   4.52907771e+12,   3.11747705e+12,
         2.14583714e+12,   1.47703318e+12,   1.01667874e+12,
         6.99805314e+11,   4.81693437e+11,   3.31561596e+11,
         2.28222109e+11,   1.57090965e+11,   1.08129626e+11,
         7.44283160e+10,   5.12308646e+10,   3.52634808e+10,
         2.42727326e+10,   1.67075267e+10,   1.15002070e+10,
         7.91587905e+09,   5.44869681e+09,   3.75047379e+09,
         2.58154457e+09,   1.77694146e+09,   1.22311309e+09,
         8.41899219e+08,   5.79500212e+08,   3.98884437e+08,
         2.74562098e+08,   1.88987934e+08,   1.18067496e+08,
         7.24569057e+07,   4.44661180e+07,   2.72884362e+07,
         1.67466553e+07,   1.02772640e+07,   6.30705975e+06,
         3.87058294e+06,   2.37534015e+06,   1.45772379e+06,
         8.94591310e+05,   5.49002229e+05,   3.36917477e+05,
         2.06763070e+05,   1.26888541e+05,   7.78702980e+04,
         4.77882657e+04,   2.93272068e+04,   1.79978295e+04,
         1.10450978e+04,   6.77827210e+03])

fig = plt.figure(figsize=(8,6))
plt.fill_between(Svalband,np.multiply(np.power(Svalband, 2),dNdS_down), np.multiply(np.power(Svalband, 2),dNdS_up), color="#DEB887", alpha=0.5)
plt.plot(Svalband,np.multiply(np.power(Svalband, 2),dNdS_down)*1e-10, lw=10, color="#DEB887", alpha=0.5,label=r'$\omega$, $1\sigma$ band')
plt.errorbar(np.multiply(Sval, 1), np.multiply(dNdS_simdet_corr, np.power(Sval, 2))/solidangle20deg, xerr= Serr, yerr= np.multiply(dNdS_simdet_corr_err, np.power(Sval, 2))/solidangle20deg, color='black', fmt='*', label='$TS>16$')
plt.plot(Svalfit, dNdS_corr*Svalfit*Svalfit,color='black',lw=1.5,label='Best Fit BPL')
plt.plot(Svalfit, dNdS_sim*Svalfit*Svalfit,color='blue', ls='--',lw=1.5,label='SIM input')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.xlabel(r'$S$ [photon/cm$^2$/s]', fontsize=18)
plt.ylabel(r'$S^2 dN/dS$ [ph/cm$^2$/s/deg$^2$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
fig.tight_layout(pad=0.5)
plt.axis([1e-12,1e-8,5e-14,1.5e-12], fontsize=18)
#plt.title(r'3FHL $S^2 dN/dS$ for $|b|>20$, TS='+str(TS_cut)+', CB')
plt.legend(loc=3,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
plt.savefig('../plot/dNdS_BPL_sim.pdf')