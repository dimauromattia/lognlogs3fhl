from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf

pigreco= np.pi


def ratio_err(num, den, err_num, err_den):
  rel_num= np.divide(err_num,num)
  rel_den= np.divide(err_den,den)
  return np.sqrt(np.power(rel_num, 2) + np.power(-rel_den, 2))*(num/den)


#OPEN EFF SIM BEST OFF

with open("../data/eff_results/sim_best_Off/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS16.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS16_off= [x[2] for x in data]
    Werr_TS16_off=[x[3] for x in data]


with open("../data/eff_results/sim_best_Off/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS25_off= [x[2] for x in data]
    Werr_TS25_off=[x[3] for x in data]
    
    
#OPEN EFF SIM BEST ALT

with open("../data/eff_results/sim_best_Alt/simall/eff_['sim_best_Alt_1', 'sim_best_Alt_2', 'sim_best_Alt_3', 'sim_best_Alt_4', 'sim_best_Alt_5']_TS16.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS16_alt= [x[2] for x in data]
    Werr_TS16_alt=[x[3] for x in data]


with open("../data/eff_results/sim_best_Alt/simall/eff_['sim_best_Alt_1', 'sim_best_Alt_2', 'sim_best_Alt_3', 'sim_best_Alt_4', 'sim_best_Alt_5']_TS25.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS25_alt= [x[2] for x in data]
    Werr_TS25_alt=[x[3] for x in data]    
    
    
    
#OPEN EFF SIM BEST OFF smin

with open("../data/eff_results/sim_best_Off_Smin/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS16.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS16_smin= [x[2] for x in data]
    Werr_TS16_smin=[x[3] for x in data]


with open("../data/eff_results/sim_best_Off_Smin/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS25_smin= [x[2] for x in data]
    Werr_TS25_smin=[x[3] for x in data]
    
#OPEN EFF SIM 1sigma BEST OFF

with open("../data/eff_results/sim_1sigmabest_Off/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS16.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS16_Off1sigma= [x[2] for x in data]
    Werr_TS16_Off1sigma=[x[3] for x in data]


with open("../data/eff_results/sim_1sigmabest_Off/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS25_Off1sigma= [x[2] for x in data]
    Werr_TS25_Off1sigma=[x[3] for x in data]    
    
 
#OPEN EFF SIM +10% ISO BEST OFF

with open("../data/eff_results/sim_best_Off_ISO10/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS16.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS16_ISO10= [x[2] for x in data] 
    Werr_TS16_ISO10=[x[3] for x in data]


with open("../data/eff_results/sim_best_Off_ISO10/simall/eff_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS25_ISO10= [x[2] for x in data]
    Werr_TS25_ISO10=[x[3] for x in data]  
    
    
smin_sist_TS16= np.divide(np.subtract(Wval_TS16_smin,Wval_TS16_off), Wval_TS16_off)
smin_sist_TS25= np.divide(np.subtract(Wval_TS25_smin,Wval_TS25_off), Wval_TS25_off)

alt_sist_TS16= np.divide(np.subtract(Wval_TS16_alt,Wval_TS16_off), Wval_TS16_off)
alt_sist_TS25= np.divide(np.subtract(Wval_TS25_alt,Wval_TS25_off), Wval_TS25_off)


Off1sigma_sist_TS16= np.divide(np.subtract(Wval_TS16_Off1sigma,Wval_TS16_off), Wval_TS16_off)
Off1sigma_sist_TS25= np.divide(np.subtract(Wval_TS25_Off1sigma,Wval_TS25_off), Wval_TS25_off)


ISO10_sist_TS16= np.divide(np.subtract(Wval_TS16_ISO10,Wval_TS16_off), Wval_TS16_off)
ISO10_sist_TS25= np.divide(np.subtract(Wval_TS25_ISO10,Wval_TS25_off), Wval_TS25_off)

smin_sist_TS16_err= ratio_err(np.subtract(Wval_TS16_smin,Wval_TS16_off), Wval_TS16_off, np.sqrt(np.add(np.power(Werr_TS16_smin, 2), np.power(Werr_TS16_off,2))), Werr_TS16_off)
smin_sist_TS25_err= ratio_err(np.subtract(Wval_TS25_smin,Wval_TS25_off), Wval_TS25_off, np.sqrt(np.add(np.power(Werr_TS25_smin, 2), np.power(Werr_TS25_off,2))), Werr_TS25_off)


alt_sist_TS16_err= ratio_err(np.subtract(Wval_TS16_alt,Wval_TS16_off), Wval_TS16_off, np.sqrt(np.add(np.power(Werr_TS16_alt, 2), np.power(Werr_TS16_off,2))), Werr_TS16_off)
alt_sist_TS25_err= ratio_err(np.subtract(Wval_TS25_alt,Wval_TS25_off), Wval_TS25_off, np.sqrt(np.add(np.power(Werr_TS25_alt, 2), np.power(Werr_TS25_off,2))), Werr_TS25_off)


Off1sigma_sist_TS16_err= ratio_err(np.subtract(Wval_TS16_Off1sigma,Wval_TS16_off), Wval_TS16_off, np.sqrt(np.add(np.power(Werr_TS16_Off1sigma, 2), np.power(Werr_TS16_off,2))), Werr_TS16_off)
Off1sigma_sist_TS25_err= ratio_err(np.subtract(Wval_TS25_Off1sigma,Wval_TS25_off), Wval_TS25_off, np.sqrt(np.add(np.power(Werr_TS25_Off1sigma, 2), np.power(Werr_TS25_off,2))), Werr_TS25_off)


ISO10_sist_TS16_err= ratio_err(np.subtract(Wval_TS16_ISO10,Wval_TS16_off), Wval_TS16_off, np.sqrt(np.add(np.power(Werr_TS16_ISO10, 2), np.power(Werr_TS16_off,2))), Werr_TS16_off)
ISO10_sist_TS25_err= ratio_err(np.subtract(Wval_TS25_ISO10,Wval_TS25_off), Wval_TS25_off, np.sqrt(np.add(np.power(Werr_TS25_ISO10, 2), np.power(Werr_TS25_off,2))), Werr_TS25_off)

#era [1:]
Sval_TS25= Sval[1:8]
Serr_TS25= Serr[1:8]
alt_sist_TS25= alt_sist_TS25[1:8]
smin_sist_TS25= smin_sist_TS25[1:8]
Off1sigma_sist_TS25= Off1sigma_sist_TS25[1:8]
ISO10_sist_TS25= ISO10_sist_TS25[1:8]
smin_sist_TS25_err= smin_sist_TS25_err[1:8]
alt_sist_TS25_err=alt_sist_TS25_err[1:8]
Off1sigma_sist_TS25_err= Off1sigma_sist_TS25_err[1:8]
ISO10_sist_TS25_err= ISO10_sist_TS25_err[1:8]

#new
Sval=Sval[:8]
Serr=Serr[:8]
alt_sist_TS16= alt_sist_TS16[:8]
smin_sist_TS16= smin_sist_TS16[:8]
Off1sigma_sist_TS16= Off1sigma_sist_TS16[:8]
ISO10_sist_TS16= ISO10_sist_TS16[:8]
smin_sist_TS16_err= smin_sist_TS16_err[:8]
alt_sist_TS16_err=alt_sist_TS16_err[:8]
Off1sigma_sist_TS16_err= Off1sigma_sist_TS16_err[:8]
ISO10_sist_TS16_err= ISO10_sist_TS16_err[:8]

syst16=np.zeros(len(smin_sist_TS16))
for t in range(len(smin_sist_TS16)):
    syst16[t]=np.mean(np.array([abs(smin_sist_TS16[t]),abs(alt_sist_TS16[t]),abs(Off1sigma_sist_TS16[t]),abs(ISO10_sist_TS16[t])]))   
print "systematics",syst16

syst25=np.zeros(len(smin_sist_TS25))
for t in range(len(smin_sist_TS25)):
    syst25[t]=np.mean(np.array([abs(smin_sist_TS25[t]),abs(alt_sist_TS25[t]),abs(Off1sigma_sist_TS25[t]),abs(ISO10_sist_TS25[t])]))   
print "systematics",syst25



fig = plt.figure(figsize=(8,6))
plt.errorbar(np.multiply(Sval, 1.07), smin_sist_TS16, xerr=Serr, yerr=smin_sist_TS16_err, fmt='d',color='red',label=r'$S_{\rm{min}}$')
#plt.errorbar(np.multiply(Sval_TS25,1.03), smin_sist_TS25, xerr=Serr_TS25, yerr=smin_sist_TS25_err, fmt='d', color='r',label='Smin TS25')
plt.errorbar(np.multiply(Sval, 1.), alt_sist_TS16, xerr=Serr, yerr=alt_sist_TS16_err, fmt='o',color= 'blue',label=r'Alt. IEM')
plt.errorbar(np.multiply(Sval, 1.14), Off1sigma_sist_TS16, xerr=Serr, yerr=Off1sigma_sist_TS16_err, fmt='p',color= 'green',label=r'$1 \sigma$ best fit')
plt.errorbar(np.multiply(Sval, 0.93), ISO10_sist_TS16, xerr=Serr, yerr=ISO10_sist_TS16_err, fmt='h',color= 'k',label=r'+10% ISO') #brown
#plt.errorbar(np.multiply(Sval, 1.0), syst16,  fmt='d', color='gold', ms=10,label='Systematics')
plt.errorbar(np.multiply(Sval, 1.0), syst16,  fmt='d', color='orange', ms=12,label='Systematics')


print Sval_TS25
plt.xscale('log')
#plt.yscale('log', nonposy='clip')
plt.ylabel(r'$(\omega - \omega_{\rm{Test}})/\omega$', fontsize=18)
plt.xlabel(r'$S$ [photon cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.grid(True,which="both",ls=":", color='0.75')
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
plt.axis([5e-12,1e-9,-0.4,0.4], fontsize=18)
plt.legend(loc=2,prop={'size':14},numpoints=1, scatterpoints=1, ncol=3)
fig.tight_layout(pad=0.5)
#plt.savefig('../plot/eff_sist_bestOff_ts16.pdf')
plt.savefig('../plot/eff_sist_bestOff_ts16_zoom.pdf')
plt.show()


fig = plt.figure(figsize=(8,6))
#plt.errorbar(np.multiply(Sval, 0.97), smin_sist_TS16, xerr=Serr, yerr=smin_sist_TS16_err, fmt='d',color='#03C03C',label=r'$S_{\rm{min}}$')
plt.errorbar(np.multiply(Sval_TS25,1.07), smin_sist_TS25, xerr=Serr_TS25, yerr=smin_sist_TS25_err, fmt='d', color='red',label=r'$S_{\rm{min}}$')
#plt.errorbar(np.multiply(Sval, 1.05), alt_sist_TS16, xerr=Serr, yerr=alt_sist_TS16_err, fmt='o',color= 'b',label=r'Alt. IEM')
plt.errorbar(Sval_TS25, alt_sist_TS25,  xerr=Serr_TS25,yerr=alt_sist_TS25_err,  fmt='o', color='blue',label=r'Alt. IEM')
plt.errorbar(np.multiply(Sval_TS25, 1.14), Off1sigma_sist_TS25, xerr=Serr_TS25, yerr=Off1sigma_sist_TS25_err, fmt='p',color= 'green',label=r'$1 \sigma$ best fit')
plt.errorbar(np.multiply(Sval_TS25, 0.93), ISO10_sist_TS25, xerr=Serr_TS25, yerr=ISO10_sist_TS25_err, fmt='h',color= 'k',label=r'+10% ISO') #era brown
#plt.errorbar(np.multiply(Sval_TS25, 1.0), syst25,  fmt='d', color='gold', ms=10,label='Systematics')
plt.errorbar(np.multiply(Sval_TS25, 1.0), syst25,  fmt='d', color='orange', ms=12,label='Systematics')

print Sval_TS25
plt.xscale('log')
#plt.yscale('log', nonposy='clip')
plt.ylabel(r'$(\omega - \omega_{\rm{Test}})/\omega$', fontsize=18)
plt.xlabel(r'$S$ [photon cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.grid(True,which="both",ls=":", color='0.75')
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
#plt.axis([5e-12,1e-8,-0.7,0.7], fontsize=18) #era
plt.axis([5e-12,1e-9,-0.4,0.4], fontsize=18)
plt.legend(loc=2,prop={'size':14},numpoints=1, scatterpoints=1, ncol=3) #era size:16
fig.tight_layout(pad=0.5)
#plt.savefig('../plot/eff_sist_bestOff_ts25.pdf')
plt.savefig('../plot/eff_sist_bestOff_ts25_zoom.pdf')
plt.show()
    
    
    