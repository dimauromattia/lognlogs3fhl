from scipy.integrate import quad
from scipy.integrate import romberg
from scipy.integrate import quadrature
from math import *
import matplotlib.pyplot as plt
import numpy as np 
from astropy import units as u
from astropy.coordinates import SkyCoord
from timeit import default_timer as timer
from scipy.interpolate import interp1d
import pyfits as pf


pigreco= np.pi

#BPL
def dNdS_BPL(S, n, i_up, i_down):
    if S>Sbreak:
        return n*np.power(S,-i_up)
    if S<=Sbreak:
        return n*np.power(S,-i_down)*np.power(Sbreak,-i_up+i_down)
      
def functheta(theta):
    return np.cos(theta)
    
solidangle20deg = 4.*(pigreco*180./180.)*romberg(functheta, pigreco*20./180.,pigreco*90./180.)*np.power(180./np.pi,2.) 



#EFFICIENCY

with open("../data/eff_102000_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS16.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval= [x[0] for x in data]
    Serr= [x[1] for x in data]
    Wval_TS16CB= [x[2] for x in data]
    Werr_TS16CB=[x[3] for x in data]
    
with open("../data/eff_102000_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS20.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS20= [x[0] for x in data]
    Serr_TS20= [x[1] for x in data]
    Wval_TS20CB= [x[2] for x in data]
    Werr_TS20CB=[x[3] for x in data]    

with open("../data/eff_102000_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0_poisson.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS25= [x[0] for x in data]
    Serr_TS25= [x[1] for x in data]
    Wval_TS25CB= [x[2] for x in data]
    Werr_TS25CB=[x[3] for x in data]

with open("../data/eff_102000_['sim1', 'sim2', 'sim3', 'sim4', 'sim5']_TS25.0_poisson_true.dat") as source_file:
    f =[x.strip() for x in source_file if x.strip()]
    data=[tuple(map(float,x.split())) for x in f[1:]]
    Sval_TS25nobias= [x[0] for x in data]
    Serr_TS25nobias= [x[1] for x in data]
    Wval_TS25CBnobias= [x[2] for x in data]
    Werr_TS25CBnobias=[x[3] for x in data]
  


fig = plt.figure(figsize=(8,6))


plt.errorbar(np.multiply(Sval, 1.04),Wval_TS16CB, xerr=Serr, yerr=Werr_TS16CB, fmt='d', color='k',label='$TS>16$ ')
plt.errorbar(np.multiply(Sval_TS20, 1.),Wval_TS20CB, xerr=Serr_TS20, yerr=Werr_TS20CB, fmt='h', color='dodgerblue', label='$TS>20$')
plt.errorbar(np.multiply(Sval_TS25, 0.96),Wval_TS25CB, xerr=Serr_TS25, yerr=Werr_TS25CB, fmt='o', color='r', label='$TS>25$')
plt.plot(Sval_TS25nobias, Wval_TS25CBnobias, 'r--')
#plt.errorbar(np.multiply(Sval, 1.08),Wval_TS16CB, xerr=Serr, yerr=Werr_TS16CB, fmt='d', color='darkviolet',label='$TS>16$ ')
#plt.errorbar(np.multiply(Sval_TS20, 1.),Wval_TS20CB, xerr=Serr_TS20, yerr=Werr_TS20CB, fmt='h', color='dodgerblue', label='$TS>20$')
#plt.errorbar(np.multiply(Sval_TS25, 0.92),Wval_TS25CB, xerr=Serr_TS25, yerr=Werr_TS25CB, fmt='o', color='darkorange', label='$TS>25$')
plt.xscale('log')
plt.yscale('log', nonposy='clip')
plt.ylabel(r'$\omega$, $\tilde{\omega}$', fontsize=22)
plt.xlabel(r'$S$ [photon cm$^{-2}$ s$^{-1}$]', fontsize=18)
plt.xticks(fontsize=22)
plt.yticks(fontsize=22)
plt.tick_params('both', length=10, width=2, which='major')
plt.tick_params('both', length=6, width=1.5, which='minor')
plt.axis([1e-12,1e-8,5e-3,3], fontsize=18)
plt.legend(loc=4,prop={'size':18},numpoints=1, scatterpoints=1)
fig.tight_layout(pad=0.5)
plt.savefig('../plot/eff_bfOffall_TS162025_obsintr.pdf')
#plt.show()
plt.close() 


 
 
 
 
